# Calcul des taux de complétion d'attributs #

### What is this repository for? ###

Ce dépôt contient une application "légère" indiquant pour chaque valeur de chaque attribut de classification de produit le pourcentage de produits par sous-sous-famille qui ont reçu cette valeur pour cet attribut.
Voir les [règles de gestion](https://galerieslafayette.jira.com/wiki/spaces/RPU/pages/257719280/Rapport+du+taux+de+saisie+des+attributs+de+Classification) pour le détail des statistiques calculées.

### How do I get set up? ###

* l'application utilise Spring Boot + Spring JDBC Template
* Pour son build elle utilise le maven-shade-plugin : simplement exécuter la commande mvn clean package
* Pour l'exéuction : java -jar rpu-completion-rate-<version>.jar --spring.profiles.active=<XXX>
(où <XXX> est désigne l'environnement d'exécution. Voir la Javadoc de la classe com.galerieslafayette.pcm.completionrate.Launcher pour plus de détail)
** Exemple : java -jar rpu-completion-rate-1.0.0.jar --spring.profiles.active=local,excel
** Ou encore : java -jar rpu-completion-rate-1.0.0.jar --spring.profiles.active=local,csv
* A VENIR : Il est prévu de pouvoir déposer dans le même dossier que le jar les fichiers de configuration application-<env>.propertiers.

