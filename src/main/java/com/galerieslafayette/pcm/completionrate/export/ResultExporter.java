package com.galerieslafayette.pcm.completionrate.export;

import java.io.Closeable;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.List;

import com.galerieslafayette.pcm.completionrate.Clock;
import com.galerieslafayette.pcm.completionrate.RunOptions;

/**
 * Stratégie "statefull" d'export des résultats des traitements de la présente application.
 *
 * @author p_nlegriel
 */
public abstract class ResultExporter implements Closeable, Clock {

    /** <i>Timestamp</i> positionnable à la fin d'un mot (par exemple un nom de fichier). */
    protected static final DateTimeFormatter TIME_STAMP_FORMAT = DateTimeFormatter.ofPattern("_yyyyMMddHHmmss");

    /** Options configurables de l'application. */
    protected final RunOptions runOptions;

    /**
     * Récupère les options de configuration de l'application.
     * 
     * @param runOptions
     */
    public ResultExporter(RunOptions runOptions) {
        this.runOptions = runOptions;
    }

    /**
     * Initialise le mécanisme de sortie des données.
     * <p>
     * Cela peut être l'ouverture en écriture d'un fichier, l'ouverture d'une connexion à une base de données, etc...
     * <p>
     * Ce mécanisme devra impérativement être refermé par la méthode {@link #close()}.
     * 
     * @throws IOException
     *             en cas d'échec d'ouverture du mécanisme de sortie des données
     */
    public abstract void init() throws IOException;

    /**
     * Écrit sur la sortie l'entête des données à produire.
     * 
     * @param columns
     *            liste des entêtes, ne doit pas être {@code null}
     * 
     * @throws IOException
     *             en cas d'incident lors de l'accès au flux de sortie des données
     */
    public abstract void writeHeader(List<String> columns) throws IOException;

    /**
     * Écrit sur la sortie une ligne complète des données à produire.
     * 
     * @param columns
     *            les données de la ligne, cette liste ne doit pas être {@code null}
     * @throws IOException
     *             en cas d'incident lors de l'accès au flux de sortie des données
     */
    public abstract void writeRow(List<String> columns) throws IOException;
}
