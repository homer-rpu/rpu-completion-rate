package com.galerieslafayette.pcm.completionrate.export;

import static org.apache.poi.ss.usermodel.CellType.NUMERIC;
import static org.apache.poi.ss.usermodel.CellType.STRING;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.galerieslafayette.pcm.completionrate.RunOptions;

/**
 * Gestion des étapes d'écriture des resultats de traitements sur un fichier Excel.
 * <p>
 * <font color="red"><b>Ce composant étant "statefull", il ne doit surtout pas être utilisé dans un contexte "multi-threads".</b></font>
 *
 * @author p_nlegriel
 */
@Component
@Profile("excel")
public class ExcelResultExporter extends ResultExporter {

    private static final Logger LOG = LoggerFactory.getLogger(ExcelResultExporter.class);

    private final File outputFile;

    private Workbook workbook;

    private int currentRowIndex;

    /** Clé pour choisir un style de cellule dans la feuille Excel en cours de construction. */
    private enum ResultCellType {
        HEADER_LEFT, HEADER_MIDDLE, HEADER_RIGHT, ROW_LEFT, ROW_MIDDLE, ROW_RIGHT
    }

    private Map<ResultCellType, CellStyle> cellStyles = new EnumMap<>(ResultCellType.class);

    @Autowired
    public ExcelResultExporter(RunOptions runOptions) {
        super(runOptions);
        final String outputFileName = runOptions.getOutputDirectory();
        final String fileNameBase = runOptions.getOutputBaseFileName();
        outputFile = Paths
                .get(outputFileName, fileNameBase + LocalDateTime.ofInstant(getCurrentInstant(), ZoneId.systemDefault()).format(TIME_STAMP_FORMAT) + ".xls")
                .toFile();
    }

    @Override
    public void init() throws IOException {
        LOG.info("Writing in output file {}", outputFile);
        currentRowIndex = 0;
        workbook = new HSSFWorkbook();
        workbook.createSheet("Taux de complétion");
    }

    @Override
    public void writeHeader(List<String> columns) {
        Sheet sheet = workbook.getSheetAt(0);
        Row header = sheet.createRow(0);
        int columnsNumber = columns.size();

        for (int i = 0; i < columnsNumber; i++) {
            header.createCell(i, STRING);
            header.getCell(i).setCellValue(columns.get(i));

            CellStyle cellStyle;
            if (i == 0) {
                cellStyle = getCellStyle(columnsNumber, i, ResultCellType.HEADER_LEFT);
            } else if (i == columnsNumber - 1) {
                cellStyle = getCellStyle(columnsNumber, i, ResultCellType.HEADER_RIGHT);
            } else {
                cellStyle = getCellStyle(columnsNumber, i, ResultCellType.HEADER_MIDDLE);
            }

            header.getCell(i).setCellStyle(cellStyle);

            workbook.getSheetAt(0).autoSizeColumn(i);
        }
    }

    @Override
    public void writeRow(List<String> columns) {
        currentRowIndex++;
        Sheet sheet = workbook.getSheetAt(0);
        Row row = sheet.createRow(currentRowIndex);
        int columnsNumber = columns.size();

        for (int i = 0; i < columnsNumber; i++) {
            if (NumberUtils.isCreatable(columns.get(i))) {
                row.createCell(i, NUMERIC);
                row.getCell(i).setCellValue(NumberUtils.createDouble(columns.get(i)));
            } else {
                row.createCell(i, STRING);
                row.getCell(i).setCellValue(columns.get(i));
            }

            CellStyle cellStyle;
            if (i == 0) {
                cellStyle = getCellStyle(columnsNumber, i, ResultCellType.ROW_LEFT);
            } else if (i == columnsNumber - 1) {
                cellStyle = getCellStyle(columnsNumber, i, ResultCellType.ROW_RIGHT);
            } else {
                cellStyle = getCellStyle(columnsNumber, i, ResultCellType.ROW_MIDDLE);
            }

            row.getCell(i).setCellStyle(cellStyle);
        }
    }

    private CellStyle getCellStyle(int columnsNumber, int columnIndex, ResultCellType cellType) {
        if (cellStyles.containsKey(cellType)) {
            return cellStyles.get(cellType);
        } else {
            CellStyle cellStyle = workbook.createCellStyle();

            switch (cellType) {
                case HEADER_LEFT:
                case HEADER_MIDDLE:
                case HEADER_RIGHT:
                    cellStyle.setBorderTop(BorderStyle.THICK);
                    cellStyle.setBorderBottom(BorderStyle.THICK);
                    centerRowCellAndSetLateralBorders(columnsNumber, columnIndex, cellStyle);
                    break;

                case ROW_LEFT:
                case ROW_MIDDLE:
                case ROW_RIGHT:
                    cellStyle.setBorderBottom(BorderStyle.THIN);
                    centerRowCellAndSetLateralBorders(columnsNumber, columnIndex, cellStyle);
                    break;

                default:
            }

            cellStyles.put(cellType, cellStyle);
            return cellStyle;
        }
    }

    /**
     * Crée des bords épais ou moyen sur une cellule en fonction de sa position sur la ligne.
     */
    private void centerRowCellAndSetLateralBorders(int columnsNumber, int columnIndex, CellStyle cellStyle) {
        cellStyle.setAlignment(HorizontalAlignment.CENTER);

        if (columnIndex == 0) {
            cellStyle.setBorderLeft(BorderStyle.THICK);
        } else {
            cellStyle.setBorderLeft(BorderStyle.THIN);
        }
        if (columnIndex == columnsNumber - 1) {
            cellStyle.setBorderRight(BorderStyle.THICK);
        } else {
            cellStyle.setBorderRight(BorderStyle.THIN);
        }
    }

    @Override
    public void close() throws IOException {
        FileOutputStream outputStream = new FileOutputStream(outputFile);
        workbook.write(outputStream);
        workbook.close();
        outputStream.close();
    }
}
