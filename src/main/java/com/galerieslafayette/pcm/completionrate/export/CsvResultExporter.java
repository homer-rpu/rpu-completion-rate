package com.galerieslafayette.pcm.completionrate.export;

import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.join;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.galerieslafayette.pcm.completionrate.RunOptions;

/**
 * Gestion "statefull" des étapes d'écriture des resultats de traitements sur un fichier CSV.
 * <p>
 * <font color="red"><b>Ce composant étant "statefull", il ne doit surtout pas être utilisé dans un contexte "multi-threads".</b></font>
 *
 * @author p_nlegriel
 */
@Component
@Profile("csv")
public class CsvResultExporter extends ResultExporter {

    private static final Logger LOG = LoggerFactory.getLogger(CsvResultExporter.class);

    protected Writer writer;

    @Autowired
    public CsvResultExporter(RunOptions runOptions) {
        super(runOptions);
    }

    @Override
    public void init() throws IOException {
        final String outputFileName = runOptions.getOutputDirectory();
        final String fileNameBase = runOptions.getOutputBaseFileName();
        final String charsetEncoding = runOptions.getOutputEncoding();
        File outputFile = Paths
                .get(outputFileName, fileNameBase + LocalDateTime.ofInstant(getCurrentInstant(), ZoneId.systemDefault()).format(TIME_STAMP_FORMAT) + ".csv")
                .toFile();
        LOG.info("Writing in output file {}", outputFile);
        writer = new OutputStreamWriter(new FileOutputStream(outputFile), Charset.forName(charsetEncoding));
    }

    @Override
    public void writeHeader(List<String> columns) throws IOException {
        writer.append('"').append(join(columns.stream().map(s -> s == null ? "" : s.replace("\"", "\"\"")).collect(toList()), "\";\"")).append('"');
    }

    @Override
    public void writeRow(List<String> columns) throws IOException {
        if (isEmpty(columns)) {
            writer.append("");
        } else {
            writer.append("\n\"").append(join(columns.stream().map(s -> s == null ? "" : s.replace("\"", "\"\"")).collect(toList()), "\";\"")).append('"');
        }
    }

    @Override
    public void close() throws IOException {
        if (writer != null) {
            writer.close();
        }
    }
}
