package com.galerieslafayette.pcm.completionrate;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

/**
 * Configuration (au sens de Spring) et démarrage de l'application de calcul des taux de complétion.
 *
 * @author p_nlegriel
 */
@SpringBootApplication
@Configuration
public class Launcher implements CommandLineRunner {

    @Bean
    @Primary
    @ConfigurationProperties(prefix = "rpu.completionrate.db.ecom")
    public DataSource ecomDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean
    @ConfigurationProperties(prefix = "rpu.completionrate.db.rpu")
    public DataSource rpuDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean
    public NamedParameterJdbcOperations ecomJdbcTemplate() {
        return new NamedParameterJdbcTemplate(ecomDataSource());
    }

    @Bean
    public NamedParameterJdbcOperations rpuJdbcTemplate() {
        return new NamedParameterJdbcTemplate(rpuDataSource());
    }

    @Autowired
    private CompletionRateComputer completionRateComputer;

    /**
     * Exécute le traitement en fonction des arguments fournis sur la ligne de commande.
     * <p>
     * Options disponibles :
     * <ul>
     * <li>{@code --spring.profiles.active=...} <b>(obligatoire)</b> - configure le traitement selon l'environnement d'exécution. Doit contenir deux éléments
     * séparés par une virgule. Le premier indique l'environnement d'exécution et le second le format de sortie. Les environnements disponibles sont :
     * <ul>
     * <li>{@code local}
     * <li>{@code integcontinue}
     * <li>{@code recette}
     * <li>{@code integration}
     * <li>{@code prep}
     * <li>{@code prod}
     * </ul>
     * Les formats de sortie disponibles sont :
     * <ul>
     * <li>{@code csv}
     * <li>{@code excel}
     * </ul>
     * <li>
     * </ul>
     */
    public static void main(String[] args) {
        SpringApplication.run(Launcher.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        completionRateComputer.createCompletionRateFile();
    }
}
