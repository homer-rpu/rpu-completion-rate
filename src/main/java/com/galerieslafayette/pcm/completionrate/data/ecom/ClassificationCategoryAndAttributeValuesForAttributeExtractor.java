package com.galerieslafayette.pcm.completionrate.data.ecom;

import static com.galerieslafayette.pcm.completionrate.data.ecom.ClassificationCategoryAndAttributeValuesForAttributeRowMapper.CLASSIFICATIONS_AND_ATTRIBUTE_VALUES_FOR_ATTRIBUTE_REQUEST;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.galerieslafayette.pcm.completionrate.data.AttributeAndValueDto;

/**
 * Extracteur des valeurs d'attributs pour un attribut et une catégorie de classification.
 *
 * @author p_nlegriel
 */
@Component
public class ClassificationCategoryAndAttributeValuesForAttributeExtractor {

    private final JdbcTemplate ecomJdbcTemplate;

    @Autowired
    public ClassificationCategoryAndAttributeValuesForAttributeExtractor(JdbcTemplate ecomJdbcTemplate) {
        this.ecomJdbcTemplate = ecomJdbcTemplate;
    }

    /**
     * Extrait, pour un code attribut de classification donné, les catégories de classifications des valeurs d'attributs pour un attribut et une catégorie de
     * classification.
     * 
     * @param attributeCode
     *            code d'attribut de classification
     * 
     * @return association entre des code de catégories de classification utilisant l'attribut fourni en argument et les valeurs possibles pour cet attribut
     *         dans chacune de ces classifications
     */
    public Map<String, Set<AttributeAndValueDto>> extractClassificationCategoriesWithTheiAttributeValuesForAttribute(final String attributeCode) {
        Map<String, Set<AttributeAndValueDto>> attributesAndValuesByCategory = new HashMap<>();
        ecomJdbcTemplate.query(CLASSIFICATIONS_AND_ATTRIBUTE_VALUES_FOR_ATTRIBUTE_REQUEST, new Object[] { attributeCode },
                new ClassificationCategoryAndAttributeValuesForAttributeRowMapper(attributeCode, attributesAndValuesByCategory));
        return attributesAndValuesByCategory;
    }
}
