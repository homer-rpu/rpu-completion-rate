package com.galerieslafayette.pcm.completionrate.data.ecom;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.stereotype.Component;

import com.galerieslafayette.pcm.completionrate.data.AttributeAndValueDto;

/**
 * Composant capable d'extraire diverses informations sur les articles pour une PSSF et un attribut de classification donnés.
 * <p>
 * Ces informations contiennent l'assignation de valeur pour l'attribut de classification concerné ainsi que de quoi reconstituer le MoCo auquel appartient
 * chaque article.
 *
 * @author p_nlegriel
 */
@Component
public class MoCoValuationDetailsExtractor {

    private static final String SEASON_CODES = "seasonCodes";
    private static final String PSSF_CODE = "pssfCode";
    private static final String ATTRIBUTE_CODE = "attributeCode";

    private static final String ATTR_NAME_COLUMN = "ATTR_NAME";
    private static final String ATTR_VALUE_CODE_COLUMN = "ATTR_VALUE_CODE";
    private static final String ATTR_VALUE_NAME_COLUMN = "ATTR_VALUE_NAME";
    private static final String UG_ARTICLE_COLUMN = "UG_ARTICLE";
    private static final String UG_PRODUCT_COLUMN = "UG_PRODUCT";
    private static final String ARTICLE_COLOR_COLUMN = "A_COLOR";

    /**
     * Requête SQL paramétrée par un code PSSF et un code d'attribut de classification pour retrouver les affectations de valeurs de l'attribut <b>par
     * article</b>.
     * <p>
     * Le résultat contient aussi des informations pour pouvoir regrouper les articles par MoCo. Il peut être utilisé aussi bien pour repérer les articles ayant
     * le flag "OnLine GL" côté RPU que pour avoir les <i>numérateurs</i> des taux de complétion par MoCos.
     */
    public static final String VALUES_BY_MOCO_BY_ATTRIBUTE_BY_PSSF_REQUEST = //
            "-- Obtain details of MoCos associated with each attribute value for a PSSF and given attribute code \r\n" //
                    + "select distinct A.CODE as " + UG_ARTICLE_COLUMN + ", P_PARENT.CODE as " + UG_PRODUCT_COLUMN + ", \r\n" //
                    + "    CALP.P_NAME as " + ATTR_NAME_COLUMN + ", CAV.P_CODE as " + ATTR_VALUE_CODE_COLUMN + ", \r\n" //
                    + "    CAVLP.P_NAME as " + ATTR_VALUE_NAME_COLUMN + ", A.P_COLOR as " + ARTICLE_COLOR_COLUMN + " \r\n" //
                    + "from CATEGORIES PSSF \r\n" //
                    + "  join CAT2PRODREL C2PR on C2PR.SOURCEPK = PSSF.PK \r\n" //
                    + "  join PRODUCTS P_PARENT on P_PARENT.PK = C2PR.TARGETPK \r\n" //
                    + "  join PRODUCTS A on A.P_BASEPRODUCT = P_PARENT.PK \r\n" //
                    + "  join PRODUCTFEATURES PF on PF.P_PRODUCT = P_PARENT.PK \r\n" //
                    + "  join CAT2ATTRREL C2AR on PF.P_CLASSIFICATIONATTRIBUTEASSIG = C2AR.PK \r\n" //
                    + "  join ENUMERATIONVALUES EV on EV.PK = C2AR.P_ATTRIBUTETYPE and EV.CODE = 'enum' \r\n" //
                    + "  join CLASSIFICATIONATTRS CA on C2AR.P_CLASSIFICATIONATTRIBUTE = CA.PK \r\n" //
                    + "  join CLASSIFICATIONATTRSLP CALP on CALP.ITEMPK = CA.PK \r\n" //
                    + "  join CLASSATTRVALUES CAV on CAV.PK = TO_NUMBER(TO_CHAR(PF.P_STRINGVALUE)) \r\n" //
                    + "  join CLASSATTRVALUESLP CAVLP on CAVLP.ITEMPK = CAV.PK \r\n" //
                    + "  join LANGUAGES L on L.PK = CAVLP.LANGPK \r\n" //
                    + "  join CATALOGVERSIONS CV on P_PARENT.P_CATALOGVERSION = CV.PK \r\n" //
                    + "  join CATALOGS C on CV.P_CATALOG = C.PK \r\n" //
                    + "  join ENUMERATIONVALUES EVAWST on EVAWST.PK = A.P_ARTICLEWORKFLOWSTATUS AND EVAWST.CODE = 'PUBLICATION'\r\n" //
                    + "  join ENUMERATIONVALUES EVS on EVS.PK = A.P_SEASON and EVS.CODE in (:" + SEASON_CODES + ") \r\n" //
                    + "where P_PARENT.P_BASEPRODUCT is null and L.ISOCODE = 'fr' and CV.P_VERSION = 'Staged' and C.P_ID = 'pcm_catalog' \r\n" //
                    + "    and PSSF.P_CODE = :" + PSSF_CODE + " and CA.P_CODE = :" + ATTRIBUTE_CODE;

    private final NamedParameterJdbcOperations ecomJdbcTemplate;

    @Autowired
    public MoCoValuationDetailsExtractor(NamedParameterJdbcOperations ecomJdbcTemplate) {
        this.ecomJdbcTemplate = ecomJdbcTemplate;
    }

    /**
     * Récupère des informations sur des articles d'une PSSF donnée avec les valorisations qui leur ont été assignées pour un attribut de classification donné.
     * 
     * @param pssf
     *            code PSSF
     * @param attributeCode
     *            code d'attribut de classification
     * @param seasonsFilter
     *            liste des codes saison sur laquelle filtrer la recherche
     * 
     * @return une liste non {@code null} et dans le désordre
     */
    public List<ArticleDetailsDto> extractArticleDetailsWithAttributeValuesForPssfAndAttribue(String pssf, String attributeCode,
            Set<String> seasonCodeFilters) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(SEASON_CODES, seasonCodeFilters);
        parameters.addValue(PSSF_CODE, pssf);
        parameters.addValue(ATTRIBUTE_CODE, attributeCode);

        return ecomJdbcTemplate.query(VALUES_BY_MOCO_BY_ATTRIBUTE_BY_PSSF_REQUEST, parameters, new RowMapper<ArticleDetailsDto>() {

            @Override
            public ArticleDetailsDto mapRow(ResultSet resultSet, int rowNumber) throws SQLException {
                return new ArticleDetailsDto(
                        new AttributeAndValueDto(attributeCode, resultSet.getString(ATTR_NAME_COLUMN), resultSet.getString(ATTR_VALUE_CODE_COLUMN),
                                resultSet.getString(ATTR_VALUE_NAME_COLUMN)),
                        resultSet.getString(UG_ARTICLE_COLUMN), resultSet.getString(UG_PRODUCT_COLUMN), resultSet.getString(ARTICLE_COLOR_COLUMN));
            }
        });
    }
}
