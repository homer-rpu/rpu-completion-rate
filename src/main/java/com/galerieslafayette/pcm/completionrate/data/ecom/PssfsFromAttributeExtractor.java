package com.galerieslafayette.pcm.completionrate.data.ecom;

import static java.util.Collections.unmodifiableMap;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.galerieslafayette.pcm.completionrate.data.AttributeAndValueDto;

/**
 * Classe d'objets capables de récupérer en base la liste des PSSF utilisant un attribut de classification donné.
 *
 * @author p_nlegriel
 */
@Component
public class PssfsFromAttributeExtractor {

    private static final String SUB_CATEGORY_CODE_COLUMN = "SUB_CATEGORY_CODE";

    /**
     * Requête SQL paramétrée par un identifiant de catégorie et retrouvant les sous-catégories dela catégorie de classification dont le code est passé en
     * paramètre.
     */
    public static final String SUB_CATEGORIES_REQUEST = "-- Obtain all sub-categories of a category which code is passed as parameter \r\n" //
            + "select distinct C_SUB.P_CODE as " + SUB_CATEGORY_CODE_COLUMN + " \r\n" //
            + "from CATEGORIES C \r\n" //
            + "  join CAT2CATREL C2CR on C2CR.SOURCEPK = C.PK \r\n" //
            + "  join CATEGORIES C_SUB on C_SUB.PK = C2CR.TARGETPK \r\n" //
            + "  join CATALOGVERSIONS CV on CV.PK = C.P_CATALOGVERSION \r\n" //
            + "  join CATALOGS CTLG on CV.P_CATALOG = CTLG.PK \r\n" //
            + "where CV.P_VERSION = 'V1.0' and CTLG.P_ID = 'ClassificationSystem' and C.P_CODE = ?";

    private final JdbcTemplate ecomJdbcTemplate;
    private final ClassificationCategoryAndAttributeValuesForAttributeExtractor classificationCategoryAndAttributeValuesForAttributeExtractor;

    @Autowired
    public PssfsFromAttributeExtractor(JdbcTemplate ecomJdbcTemplate,
            ClassificationCategoryAndAttributeValuesForAttributeExtractor classificationCategoryAndAttributeValuesForAttributeExtractor) {
        this.ecomJdbcTemplate = ecomJdbcTemplate;
        this.classificationCategoryAndAttributeValuesForAttributeExtractor = classificationCategoryAndAttributeValuesForAttributeExtractor;
    }

    /**
     * Recherche en base de données toutes les PSSF pour lesquelles un code d'attribut de classication donné s'applique (et pour chaque PSSF trouvée la liste
     * des valeurs d'attributs possibles est fournie).
     * 
     * @return une <i>map</i> immutable contenant en clé le code code PSSF et en valeur les valeurs d'attributs possibles pour cette PSSF
     */
    public Map<String, Set<AttributeAndValueDto>> extractPssfsAndAttributeValues(final String attributeCode) {
        // Récupérer les catégories directement concernées par l'attribut de classification ainsi que les valeurs d'attributs dans chaque cas
        final Map<String, Set<AttributeAndValueDto>> attributesAndValuesByCategory = classificationCategoryAndAttributeValuesForAttributeExtractor
                .extractClassificationCategoriesWithTheiAttributeValuesForAttribute(attributeCode);

        // Parcours récursif de l'arbre des catégories (après avoir supprimé les doublons dans la liste)
        Map<String, Set<AttributeAndValueDto>> pssfs = new HashMap<>();
        exploreCategoriesAndSubCategories(attributesAndValuesByCategory, pssfs);
        return unmodifiableMap(pssfs);
    }

    /**
     * Explore récursivement l'arborescence des catégories de classification pour trouver toutes celles correspondant à des PSSF.
     * 
     * @param attributesAndValuesByCategory
     *            contient des codes de catégories de classification
     * @param pssfs
     *            complétée par le traitement
     */
    private void exploreCategoriesAndSubCategories(Map<String, Set<AttributeAndValueDto>> attributesAndValuesByCategory,
            Map<String, Set<AttributeAndValueDto>> pssfs) {
        if (attributesAndValuesByCategory.isEmpty()) {
            return;
        }

        for (Entry<String, Set<AttributeAndValueDto>> categoryEntry : attributesAndValuesByCategory.entrySet()) {
            String categoryCode = categoryEntry.getKey();
            if (categoryCode.startsWith("PSSF")) {
                pssfs.put(categoryCode, categoryEntry.getValue());
            } else {
                List<String> subCategories = executeMonoColumnQueryWithOneParameter(SUB_CATEGORIES_REQUEST, categoryCode, SUB_CATEGORY_CODE_COLUMN);
                Map<String, Set<AttributeAndValueDto>> attributesAndValuesForSubCategories = new HashMap<>();
                for (String subCategory : subCategories) {
                    // Transmettre les bonnes valeurs d'attributs à la recherche des sous-catégories
                    attributesAndValuesForSubCategories.put(subCategory, categoryEntry.getValue());
                }
                exploreCategoriesAndSubCategories(attributesAndValuesForSubCategories, pssfs);
            }
        }
    }

    private List<String> executeMonoColumnQueryWithOneParameter(final String request, final String parameter, final String columnName) {
        return ecomJdbcTemplate.query(request, new Object[] { parameter }, new RowMapper<String>() {

            @Override
            public String mapRow(ResultSet resultSet, int rowNumber) throws SQLException {
                return resultSet.getString(columnName);
            }
        });
    }
}
