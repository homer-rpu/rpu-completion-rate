package com.galerieslafayette.pcm.completionrate.data.ecom;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.stereotype.Component;

/**
 * Calculateur de nombre de produits ou de nombre de MoCos mappés sur une PSSF donnée.
 *
 * @author p_nlegriel
 */
@Component
public class ProductAndMoCoNumbersForPssfExtractor {

    private static final String SEASON_CODES = "seasonCodes";
    private static final String PSSF_CODE = "pssfCode";

    private static final String PRODUCTS_NUMER_COLUMN = "PRODUCTS_NUMBER";
    private static final String MOCOS_NUMER_COLUMN = "MOCOS_NUMBER";

    // TODO nlegriel:
    /**
     * Requête SQL paramétrée par un code de PSSF et fournissant le nombre de produits appartenant à cette PSSF.
     */
    public static final String PRODUCTS_NUMER_FOR_PSSF_REQUEST = "-- Count products belonging to a PSSF \r\n" //
            + "select count(P.PK) as " + PRODUCTS_NUMER_COLUMN + " \r\n" //
            + "from PRODUCTS P \r\n" //
            + "  join CAT2PRODREL C2PR on C2PR.TARGETPK = P.PK \r\n" //
            + "  join CATEGORIES C_PSSF on C_PSSF.PK = C2PR.SOURCEPK \r\n" //
            + "  join CATALOGVERSIONS CV on CV.PK = C_PSSF.P_CATALOGVERSION \r\n" //
            + "  join CATALOGS C on CV.P_CATALOG = C.PK \r\n" //
            + "  join ENUMERATIONVALUES EV on EV.PK = P.P_PRODUCTWORKFLOWSTATUS and EV.CODE='PUBLICATION'"
            + "  join ENUMERATIONVALUES EVS on EVS.PK = P.P_SEASON and EVS.CODE in (:" + SEASON_CODES + ") \r\n" //
            + "where P.P_BASEPRODUCT is null and CV.P_VERSION = 'Staged' and C.P_ID = 'pcm_catalog' and C_PSSF.P_CODE = :" + PSSF_CODE;

    /**
     * Requête SQL paramétrée par un code de PSSF et fournissant le nombre de MoCos appartenant à cette PSSF.
     */
    public static final String MOCOS_NUMER_FOR_PSSF_REQUEST = "-- Count MoCos belonging to a PSSF \r\n" //
            + "select count(*) as " + MOCOS_NUMER_COLUMN + " \r\n" //
            + "  from (select distinct P_PARENT.CODE || '-' || P.P_COLOR \r\n" //
            + "    from PRODUCTS P \r\n" //
            + "      join PRODUCTS P_PARENT on P_PARENT.PK = P.P_BASEPRODUCT\r\n" //
            + "      join CAT2PRODREL C2PR on C2PR.TARGETPK = P_PARENT.PK \r\n" //
            + "      join CATEGORIES C_PSSF on C_PSSF.PK = C2PR.SOURCEPK \r\n" //
            + "      join CATALOGVERSIONS CV on CV.PK = C_PSSF.P_CATALOGVERSION \r\n" //
            + "      join CATALOGS C on CV.P_CATALOG = C.PK \r\n" //
            + "      join ENUMERATIONVALUES EV on EV.PK = P_PARENT.P_PRODUCTWORKFLOWSTATUS and EV.CODE='PUBLICATION'" //
            + "      join ENUMERATIONVALUES EVS on EVS.PK = P.P_SEASON and EVS.CODE in (:" + SEASON_CODES + ") \r\n" //
            + "    where CV.P_VERSION = 'Staged' and C.P_ID = 'pcm_catalog' and C_PSSF.P_CODE = :" + PSSF_CODE + ")";

    private final NamedParameterJdbcOperations ecomJdbcTemplate;

    @Autowired
    public ProductAndMoCoNumbersForPssfExtractor(NamedParameterJdbcOperations ecomJdbcTemplate) {
        this.ecomJdbcTemplate = ecomJdbcTemplate;
    }

    /**
     * Compte le nombre de MoCos appartenant à une PSSF donnée.
     * 
     * @param pssf
     *            code d'une PSSF
     * @param seasonCodeFilters
     *            liste des codes saison sur laquelle filtrer la recherche
     */
    public int extractProductsNumber(String pssf, Set<String> seasonCodeFilters) {
        return executeMonoColumnQueryWithOneParameter(PRODUCTS_NUMER_FOR_PSSF_REQUEST, pssf, seasonCodeFilters, PRODUCTS_NUMER_COLUMN).get(0);
    }

    /**
     * Compte le nombre de produits appartenant à une PSSF donnée.
     * 
     * @param pssf
     *            code d'une PSSF
     * @param seasonCodeFilters
     *            liste des codes saison sur laquelle filtrer la recherche
     */
    public int extractMoCosNumber(String pssf, Set<String> seasonCodeFilters) {
        return executeMonoColumnQueryWithOneParameter(MOCOS_NUMER_FOR_PSSF_REQUEST, pssf, seasonCodeFilters, MOCOS_NUMER_COLUMN).get(0);
    }

    private List<Integer> executeMonoColumnQueryWithOneParameter(final String request, final String parameter1, Set<String> parameter2,
            final String columnName) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(SEASON_CODES, parameter2);
        parameters.addValue(PSSF_CODE, parameter1);

        return ecomJdbcTemplate.query(request, parameters, new RowMapper<Integer>() {

            @Override
            public Integer mapRow(ResultSet resultSet, int rowNumber) throws SQLException {
                return resultSet.getInt(columnName);
            }
        });
    }
}
