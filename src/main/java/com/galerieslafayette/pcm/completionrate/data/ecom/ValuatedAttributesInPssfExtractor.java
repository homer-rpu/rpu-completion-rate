package com.galerieslafayette.pcm.completionrate.data.ecom;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.stereotype.Component;

import com.galerieslafayette.pcm.completionrate.data.AttributeAndValueDto;

/**
 * Composant capable de récupérer les produits ou les MoCos ayant reçu une affecation de valeur sur un attribut donné pour une PSSF donnée.
 *
 * @author p_nlegriel
 */
@Component
public class ValuatedAttributesInPssfExtractor {

    private static final String SEASON_CODES = "seasonCodes";
    private static final String PSSF_CODE = "pssfCode";
    private static final String ATTRIBUTE_CODE = "attributeCode";

    private static final String ATTR_NAME_COLUMN = "ATTR_NAME";
    private static final String ATTR_VALUE_CODE_COLUMN = "ATTR_VALUE_CODE";
    private static final String ATTR_VALUE_NAME_COLUMN = "ATTR_VALUE_NAME";

    /**
     * Requête SQL paramétrée par un code PSSF et un code d'attribut de classification pour retrouver les affectations de valeurs de l'attribut <b>par
     * produit</b>.
     * <p>
     * Son résultat permet le calcul des <i>numérateurs</i> des taux de complétions d'affectation de valeurs d'attributs sur les produits.
     */
    public static final String VALUES_BY_ATTRIBUTE_BY_PSSF_REQUEST = //
            "-- Obtain details of products associated with each attribute value for a PSSF and given attribute code \r\n" //
                    + "select distinct PRODUCT.CODE as UG_PRODUCT, CALP.P_NAME as " + ATTR_NAME_COLUMN + ", \r\n" //
                    + "    CAV.P_CODE as " + ATTR_VALUE_CODE_COLUMN + ", CAVLP.P_NAME as " + ATTR_VALUE_NAME_COLUMN + " \r\n" //
                    + "from CATEGORIES PSSF \r\n" //
                    + "  join CAT2PRODREL C2PR on C2PR.SOURCEPK = PSSF.PK \r\n" //
                    + "  join PRODUCTS PRODUCT on PRODUCT.PK = C2PR.TARGETPK \r\n" //
                    + "  join PRODUCTFEATURES PF on PF.P_PRODUCT = PRODUCT.PK \r\n" //
                    + "  join CAT2ATTRREL C2AR on PF.P_CLASSIFICATIONATTRIBUTEASSIG = C2AR.PK \r\n" //
                    + "  join ENUMERATIONVALUES EV on EV.PK = C2AR.P_ATTRIBUTETYPE and EV.CODE='enum' \r\n" //
                    + "  join CLASSIFICATIONATTRS CA on C2AR.P_CLASSIFICATIONATTRIBUTE = CA.PK \r\n" //
                    + "  join CLASSIFICATIONATTRSLP CALP on CALP.ITEMPK = CA.PK \r\n" //
                    + "  join CLASSATTRVALUES CAV on CAV.PK = TO_NUMBER(TO_CHAR(PF.P_STRINGVALUE)) \r\n" //
                    + "  join CLASSATTRVALUESLP CAVLP on CAVLP.ITEMPK = CAV.PK \r\n" //
                    + "  join LANGUAGES L on L.PK = CAVLP.LANGPK \r\n" //
                    + "  join CATALOGVERSIONS CV on PRODUCT.P_CATALOGVERSION = CV.PK \r\n" //
                    + "  join CATALOGS C on CV.P_CATALOG = C.PK \r\n" //
                    + "  join ENUMERATIONVALUES EV on EV.PK = PRODUCT.P_PRODUCTWORKFLOWSTATUS and EV.CODE='PUBLICATION'" //
                    + "  join ENUMERATIONVALUES EVS on EVS.PK = PRODUCT.P_SEASON and EVS.CODE in (:" + SEASON_CODES + ") \r\n" //
                    + "where PRODUCT.P_BASEPRODUCT is null and L.ISOCODE = 'fr' and CV.P_VERSION = 'Staged' and C.P_ID = 'pcm_catalog' \r\n" //
                    + "    and PSSF.P_CODE = :" + PSSF_CODE + " and CA.P_CODE = :" + ATTRIBUTE_CODE;

    private final NamedParameterJdbcOperations ecomJdbcTemplate;

    @Autowired
    public ValuatedAttributesInPssfExtractor(NamedParameterJdbcOperations ecomJdbcTemplate) {
        this.ecomJdbcTemplate = ecomJdbcTemplate;
    }

    /**
     * Récupère les valorisations de <b>produits</b> pour une PSSF donnée et un attribut de classification donné.
     * 
     * @param pssf
     *            code PSSF
     * @param attributeCode
     *            code d'attribut de classification s'appliquant sur la PSSF
     * @param seasonCodeFilters
     *            liste des codes saison sur laquelle filtrer la recherche
     * 
     * @return une liste de couples <i>[attribut; valeur d'attribut]</i>
     */
    public List<AttributeAndValueDto> extractValuesByAttributeForPssf(String pssf, String attributeCode, Set<String> seasonCodeFilters) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(SEASON_CODES, seasonCodeFilters);
        parameters.addValue(PSSF_CODE, pssf);
        parameters.addValue(ATTRIBUTE_CODE, attributeCode);

        return ecomJdbcTemplate.query(VALUES_BY_ATTRIBUTE_BY_PSSF_REQUEST, parameters, new RowMapper<AttributeAndValueDto>() {

            @Override
            public AttributeAndValueDto mapRow(ResultSet resultSet, int rowNumber) throws SQLException {
                return new AttributeAndValueDto(attributeCode, resultSet.getString(ATTR_NAME_COLUMN), resultSet.getString(ATTR_VALUE_CODE_COLUMN),
                        resultSet.getString(ATTR_VALUE_NAME_COLUMN));
            }
        });
    }
}
