package com.galerieslafayette.pcm.completionrate.data.ecom;

import static java.util.Objects.requireNonNull;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.springframework.jdbc.core.RowMapper;

import com.galerieslafayette.pcm.completionrate.data.AttributeAndValueDto;

/**
 * Ce {@link RowMapper} <b><i><font color="red">stateful</font></i></b> extrait, pour un attribut de classification donné, les catégories de classification qui
 * l'utilisent avec pour chacune d'entre elles, les valeurs d'attribut utilisables.
 * 
 * @param attributeCode
 *            un code d'attribut de classification
 * @return un regroupement de valeurs d'attribut par code de catégorie de classification
 * 
 *
 * @author p_nlegriel
 */
public class ClassificationCategoryAndAttributeValuesForAttributeRowMapper implements RowMapper<String> {

    private static final String CATEGORY_CODE_COLUMN = "CATEGORY_CODE";
    private static final String ATTRIBUTE_NAME_COLUMN = "ATTR_NAME";
    private static final String ATTRIBUTE_VALUE_CODE_COLUMN = "ATTR_VALUE_CODE";
    private static final String ATTRIBUTE_VALUE_NAME_COLUMN = "ATTR_VALUE_NAME";

    /**
     * Requête SQL paramétrée par le code d'attribut de classification retrouvant toutes les classifications du catalogue <i>Staged</i> utilisant un attribut de
     * classification donné (les valeurs possibles pour l'attribut de classification sont associées au résultat).
     */
    public static final String CLASSIFICATIONS_AND_ATTRIBUTE_VALUES_FOR_ATTRIBUTE_REQUEST = //
            "-- Obtain all categories using the attribute with code passed as parameter (attribute values are associated to result) \r\n" //
                    + "select distinct CLA.P_CODE as " + CATEGORY_CODE_COLUMN + ", CALP.P_NAME as " + ATTRIBUTE_NAME_COLUMN + ", \r\n" //
                    + "    CAV.P_CODE as " + ATTRIBUTE_VALUE_CODE_COLUMN + ", CAVLP.P_NAME as " + ATTRIBUTE_VALUE_NAME_COLUMN + " \r\n" //
                    + "from CLASSIFICATIONATTRS CA \r\n" //
                    + "  join CLASSIFICATIONATTRSLP CALP on CALP.ITEMPK = CA.PK \r\n" //
                    + "  join CAT2ATTRREL C2AR on C2AR.P_CLASSIFICATIONATTRIBUTE = CA.PK \r\n" //
                    + "  join CATEGORIES CLA on CLA.PK = C2AR.P_CLASSIFICATIONCLASS \r\n" //
                    + "  join ATTR2VALUEREL A2VR on A2VR.P_ATTRIBUTEASSIGNMENT = C2AR.PK \r\n" //
                    + "  join CLASSATTRVALUES CAV on CAV.PK = A2VR.P_VALUE \r\n" //
                    + "  join CLASSATTRVALUESLP CAVLP on CAVLP.ITEMPK = CAV.PK \r\n" //
                    + "  join LANGUAGES L on L.PK = CAVLP.LANGPK and L.PK = CALP.LANGPK \r\n" //
                    + "  join CATALOGVERSIONS CV on CV.PK = CA.P_SYSTEMVERSION \r\n" //
                    + "  join CATALOGS CTLG on CV.P_CATALOG = CTLG.PK \r\n" //
                    + "where L.ISOCODE = 'fr' and CV.P_VERSION = 'V1.0' and CTLG.P_ID = 'ClassificationSystem' and CA.P_CODE = ?";

    private final String attributeCode;
    private final Map<String, Set<AttributeAndValueDto>> attributesAndValuesByCategory;

    /**
     * Associe les deux éléments d'état indispensables au fonctionnement du présent <i>RowMapper</i>.
     * 
     * @param attributeCode
     *            code d'attribut de classification
     * @param attributesAndValuesByCategory
     *            <i>map</i> destinée à être complétée progressivement par la méthode {@link #mapRow(ResultSet, int)} et regroupant pour chaque code de
     *            catégorie de classification la liste des valeurs pouvant être affectées à l'attribut de classification fourni en premier argument
     */
    public ClassificationCategoryAndAttributeValuesForAttributeRowMapper(String attributeCode,
            Map<String, Set<AttributeAndValueDto>> attributesAndValuesByCategory) {
        requireNonNull(attributesAndValuesByCategory);
        this.attributeCode = attributeCode;
        this.attributesAndValuesByCategory = attributesAndValuesByCategory;
    }

    @Override
    public String mapRow(ResultSet resultSet, int rowNumber) throws SQLException {
        final String category = resultSet.getString(CATEGORY_CODE_COLUMN);
        final String attributeName = resultSet.getString(ATTRIBUTE_NAME_COLUMN);
        final String attributeValueCode = resultSet.getString(ATTRIBUTE_VALUE_CODE_COLUMN);
        final String attributeValueName = resultSet.getString(ATTRIBUTE_VALUE_NAME_COLUMN);

        Set<AttributeAndValueDto> attributesAndValues = attributesAndValuesByCategory.computeIfAbsent(category, k -> new HashSet<>());
        attributesAndValues.add(new AttributeAndValueDto(attributeCode, attributeName, attributeValueCode, attributeValueName));

        return category;
    }
}
