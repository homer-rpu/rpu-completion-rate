package com.galerieslafayette.pcm.completionrate.data.ecom;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.galerieslafayette.pcm.completionrate.data.CategoryStructureDto;

/**
 * Détermination de la hiérarchie de catégories PCM pour une PSSF.
 *
 * @author p_nlegriel
 */
@Component
public class ClassificationStructureExtractor {

    private static final String PSSF_CODE_COLUMN = "PSSF_CODE";
    private static final String PSSF_NAME_COLUMN = "PSSF_NAME";
    private static final String PSF_CODE_COLUMN = "PSF_CODE";
    private static final String PSF_NAME_COLUMN = "PSF_NAME";
    private static final String PF_CODE_COLUMN = "PF_CODE";
    private static final String PF_NAME_COLUMN = "PF_NAME";
    private static final String PU_CODE_COLUMN = "PU_CODE";
    private static final String PU_NAME_COLUMN = "PU_NAME";

    public static final String PCM_STRUCTURE_FOR_PSSF_REQUEST = "-- Obtain PCM categories hierarchy for a PSSF \r\n" //
            + "select C_PSSF.P_CODE as " + PSSF_CODE_COLUMN + ", CLP_PSSF.P_NAME as " + PSSF_NAME_COLUMN + ", \r\n" //
            + "    C_PSF.P_CODE as " + PSF_CODE_COLUMN + ", CLP_PSF.P_NAME as " + PSF_NAME_COLUMN + ", \r\n" //
            + "    C_PF.P_CODE as " + PF_CODE_COLUMN + ", CLP_PF.P_NAME as " + PF_NAME_COLUMN + ", \r\n" //
            + "    C_U.P_CODE as " + PU_CODE_COLUMN + ", CLP_U.P_NAME as " + PU_NAME_COLUMN + " \r\n" //
            + "from CATEGORIES C_PSSF \r\n" //
            + "  -- PSSF level \r\n" //
            + "  join CATEGORIESLP CLP_PSSF on CLP_PSSF.ITEMPK = C_PSSF.PK \r\n" //
            + "\r\n" //
            + "  -- PSF level \r\n" //
            + "  join CAT2CATREL C2CR_PSF on C2CR_PSF.TARGETPK = C_PSSF.PK \r\n" //
            + "  join CATEGORIES C_PSF on C_PSF.PK = C2CR_PSF.SOURCEPK \r\n" //
            + "  join CATEGORIESLP CLP_PSF on CLP_PSF.ITEMPK = C_PSF.PK \r\n" //
            + "\r\n" //
            + "  -- PF level \r\n" //
            + "  join CAT2CATREL C2CR_PF on C2CR_PF.TARGETPK = C_PSF.PK \r\n" //
            + "  join CATEGORIES C_PF on C_PF.PK = C2CR_PF.SOURCEPK \r\n" //
            + "  join CATEGORIESLP CLP_PF on CLP_PF.ITEMPK = C_PF.PK \r\n" //
            + "\r\n" //
            + "  -- PU level \r\n" //
            + "  join CAT2CATREL C2CR_U on C2CR_U.TARGETPK = C_PF.PK \r\n" //
            + "  join CATEGORIES C_U on C_U.PK = C2CR_U.SOURCEPK \r\n" //
            + "  join CATEGORIESLP CLP_U on CLP_U.ITEMPK = C_U.PK \r\n" //
            + "\r\n" //
            + "  join CATALOGVERSIONS CV on CV.PK = C_PSSF.P_CATALOGVERSION and CV.PK = C_PSF.P_CATALOGVERSION and CV.PK = C_PF.P_CATALOGVERSION and CV.PK = C_U.P_CATALOGVERSION \r\n" //
            + "  join CATALOGS C on CV.P_CATALOG = C.PK \r\n" //
            + "\r\n" //
            + "where CV.P_VERSION = 'Staged' and C.P_ID = 'pcm_catalog' and C_PSSF.P_CODE = ?";

    private final JdbcTemplate ecomJdbcTemplate;

    @Autowired
    public ClassificationStructureExtractor(JdbcTemplate ecomJdbcTemplate) {
        this.ecomJdbcTemplate = ecomJdbcTemplate;
    }

    /**
     * Fournit les informations sur la hiérarchie des catégories PCM supérieure à une PSSF donnée.
     * 
     * @param pssf
     *            code d'une PSSF
     * 
     * @return un <i>DTO</i> immutable rassemblant les informations recherchées ou {@code null} si le code PSSF fourni est inexistant
     */
    public CategoryStructureDto extractPcmStructureForPssf(String pssf) {
        List<CategoryStructureDto> result = ecomJdbcTemplate.query(PCM_STRUCTURE_FOR_PSSF_REQUEST, new Object[] { pssf }, new RowMapper<CategoryStructureDto>() {

            @Override
            public CategoryStructureDto mapRow(ResultSet resultSet, int rowNumber) throws SQLException {
                return new CategoryStructureDto(resultSet.getString(PU_CODE_COLUMN), resultSet.getString(PU_NAME_COLUMN), resultSet.getString(PF_CODE_COLUMN),
                        resultSet.getString(PF_NAME_COLUMN), resultSet.getString(PSF_CODE_COLUMN), resultSet.getString(PSF_NAME_COLUMN),
                        resultSet.getString(PSSF_CODE_COLUMN), resultSet.getString(PSSF_NAME_COLUMN));
            }
        });

        if (result.isEmpty()) {
            return null;
        }
        return result.get(0);
    }
}
