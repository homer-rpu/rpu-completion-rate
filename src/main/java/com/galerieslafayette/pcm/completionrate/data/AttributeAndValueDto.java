package com.galerieslafayette.pcm.completionrate.data;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Classe d'objets de transport immutables contenant les informations essentielles sur un attribut de classification au sens d'Hybris, <b>avec une valeur qui lui
 * est potentiellement assignable pour un produit donné</b>.
 *
 * @author p_nlegriel
 */
public class AttributeAndValueDto {

    private final String attributeCode;
    private final String attributeName;
    private final String attributeValueCode;
    private final String attributeValueName;

    public AttributeAndValueDto(String attributeCode, String attributeName, String attributeValueCode, String attributeValueName) {
        this.attributeCode = attributeCode;
        this.attributeName = attributeName;
        this.attributeValueCode = attributeValueCode;
        this.attributeValueName = attributeValueName;
    }

    public String getAttributeCode() {
        return attributeCode;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public String getAttributeValueCode() {
        return attributeValueCode;
    }

    public String getAttributeValueName() {
        return attributeValueName;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(attributeCode).append(attributeValueCode).build();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        AttributeAndValueDto other = (AttributeAndValueDto) obj;
        return new EqualsBuilder().append(attributeCode, other.getAttributeCode()).append(attributeValueCode, other.getAttributeValueCode()).build();
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
