package com.galerieslafayette.pcm.completionrate.data.ecom;

/**
 * Classe d'objets de transport immutables contenant les informations sur un article (comme {@link ArticleDetailsDto}) mais avec l'information supplémentaire
 * indiquant si l'article est en ligne ou nom sur le site Galeries Lafayette.
 *
 * @author p_nlegriel
 */
public class ArticleDetailsWithFlagOnLineDto extends ArticleDetailsDto {

    private final boolean onLine;

    public ArticleDetailsWithFlagOnLineDto(ArticleDetailsDto articleDetailsDto, boolean isOnLine) {
        super(articleDetailsDto.getAttributeAndValue(), articleDetailsDto.getUgArticle(), articleDetailsDto.getUgProduct(), articleDetailsDto.getColorPk());
        this.onLine = isOnLine;
    }

    public boolean isOnLine() {
        return onLine;
    }
}
