package com.galerieslafayette.pcm.completionrate.data;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Classe d'objets de transport immutables contenant les informations la structure d'une PSSF dans les classifications "PCM".
 *
 * @author p_nlegriel
 */
public class CategoryStructureDto {

    private final String universeCode;
    private final String universeName;

    private final String pfCode;
    private final String pfName;

    private final String psfCode;
    private final String psfName;

    private final String pssfCode;
    private final String pssfName;

    public CategoryStructureDto(String universeCode, String universeName, String pfCode, String pfName, String psfCode, String psfName, String pssfCode,
            String pssfName) {
        this.universeCode = universeCode;
        this.universeName = universeName;
        this.pfCode = pfCode;
        this.pfName = pfName;
        this.psfCode = psfCode;
        this.psfName = psfName;
        this.pssfCode = pssfCode;
        this.pssfName = pssfName;
    }

    public String getUniverseCode() {
        return universeCode;
    }

    public String getUniverseName() {
        return universeName;
    }

    public String getPfCode() {
        return pfCode;
    }

    public String getPfName() {
        return pfName;
    }

    public String getPsfCode() {
        return psfCode;
    }

    public String getPsfName() {
        return psfName;
    }

    public String getPssfCode() {
        return pssfCode;
    }

    public String getPssfName() {
        return pssfName;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(pssfCode).build();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        CategoryStructureDto other = (CategoryStructureDto) obj;
        return new EqualsBuilder().append(pssfCode, other.getPssfCode()).build();
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
