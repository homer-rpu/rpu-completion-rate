package com.galerieslafayette.pcm.completionrate.data.ecom;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.galerieslafayette.pcm.completionrate.data.AttributeAndValueDto;

/**
 * Classe d'objets de transport immutables contenant les informations sur un article.
 * <p>
 * Ces informations permettent notamment de connaître la PSSF, le MoCo et la valeur assignée pour un attribut de classification donné.
 *
 * @author p_nlegriel
 */
public class ArticleDetailsDto {

    private final AttributeAndValueDto attributeAndValue;
    private final String ugArticle;
    private final String ugProduct;
    private final String colorPk;

    public ArticleDetailsDto(AttributeAndValueDto attributeAndValue, String ugArticle, String ugProduct, String colorPk) {
        this.attributeAndValue = attributeAndValue;
        this.ugArticle = ugArticle;
        this.ugProduct = ugProduct;
        this.colorPk = colorPk;
    }

    public AttributeAndValueDto getAttributeAndValue() {
        return attributeAndValue;
    }

    public String getUgArticle() {
        return ugArticle;
    }

    public String getUgProduct() {
        return ugProduct;
    }

    public String getColorPk() {
        return colorPk;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
