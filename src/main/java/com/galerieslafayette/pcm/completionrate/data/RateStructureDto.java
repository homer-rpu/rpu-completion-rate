package com.galerieslafayette.pcm.completionrate.data;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * DTO <b>mutable</b> permettant la mise à jour des résultats partiels au dours d'un calcul de totaux et de taux.
 *
 * @author p_nlegriel
 */
public class RateStructureDto {

    private static final DecimalFormat PERCENTAGE = new DecimalFormat("##0.0 %", DecimalFormatSymbols.getInstance(Locale.ENGLISH));

    /**
     * Pour afficher qu'une valeur numérique ne peut pas être calculée (par exemple un pourcentage avec un dénominateur à 0).
     */
    public static final String NS_VALUE = "NS";

    private final AttributeAndValueDto attributeAndValue;
    private final CategoryStructureDto categoryStructure;
    private final boolean onLineGl;

    private int mappedMoCosNumber;
    private int valuatedMoCosNumber;
    private int mappedProductsNumber;
    private int valuatedProductsNumber;

    public RateStructureDto(CategoryStructureDto categoryStructure, AttributeAndValueDto attributeAndValue, boolean isOnLineGl, int mappedProductsNumber,
            int mappedMoCosNumber, int valuatedProductsNumber, int valuatedMoCosNumber) {
        if (attributeAndValue == null) {
            throw new IllegalArgumentException("Il manque le premier argument au constructeur de la classe " + getClass());
        }
        if (categoryStructure == null) {
            throw new IllegalArgumentException("Il manque le second argument au constructeur de la classe " + getClass());
        }
        this.attributeAndValue = attributeAndValue;
        this.categoryStructure = categoryStructure;
        this.onLineGl = isOnLineGl;
        this.mappedProductsNumber = mappedProductsNumber;
        this.mappedMoCosNumber = mappedMoCosNumber;
        this.valuatedProductsNumber = valuatedProductsNumber;
        this.valuatedMoCosNumber = valuatedMoCosNumber;
    }

    public String getUniverse() {
        return categoryStructure.getUniverseCode();
    }

    public String getPf() {
        return categoryStructure.getPfCode();
    }

    public String getPsf() {
        return categoryStructure.getPsfCode();
    }

    public String getPssf() {
        return categoryStructure.getPssfCode();
    }

    public String getPssfLabel() {
        return categoryStructure.getPssfCode() + " - " + categoryStructure.getPssfName();
    }

    public String getPsfLabel() {
        return categoryStructure.getPsfCode() + " - " + categoryStructure.getPsfName();
    }

    public String getPfLabel() {
        return categoryStructure.getPfCode() + " - " + categoryStructure.getPfName();
    }

    public String getUniverseLabel() {
        return categoryStructure.getUniverseCode() + " - " + categoryStructure.getUniverseName();
    }

    public String getAttributeCode() {
        return attributeAndValue.getAttributeCode();
    }

    public String getAttributeName() {
        return attributeAndValue.getAttributeName();
    }

    public String getAttributeValueCode() {
        return attributeAndValue.getAttributeValueCode();
    }

    public String getAttributeValueName() {
        return attributeAndValue.getAttributeValueName();
    }

    public boolean isOnLineGl() {
        return onLineGl;
    }

    public String getMappedMoCosNumber() {
        return String.valueOf(mappedMoCosNumber);
    }

    public String getValuatedMoCosNumber() {
        return String.valueOf(valuatedMoCosNumber);
    }

    public String getValuatedMocosRate() {
        if (mappedMoCosNumber == 0) {
            return NS_VALUE;
        } else {
            return PERCENTAGE.format(BigDecimal.valueOf(valuatedMoCosNumber).setScale(3).divide(BigDecimal.valueOf(mappedMoCosNumber), RoundingMode.HALF_UP));
        }
    }

    public String getMappedProductsNumber() {
        return String.valueOf(mappedProductsNumber);
    }

    public String getValuatedProductsNumber() {
        return String.valueOf(valuatedProductsNumber);
    }

    public String getValuatedProductsRate() {
        if (mappedProductsNumber == 0) {
            return NS_VALUE;
        } else {
            return PERCENTAGE
                    .format(BigDecimal.valueOf(valuatedProductsNumber).setScale(3).divide(BigDecimal.valueOf(mappedProductsNumber), RoundingMode.HALF_UP));
        }
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(categoryStructure.getPssfCode()).append(attributeAndValue.getAttributeCode())
                .append(attributeAndValue.getAttributeValueCode()).build();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        RateStructureDto other = (RateStructureDto) obj;

        return new EqualsBuilder().append(getPssf(), other.getPssf()).append(getAttributeCode(), other.getAttributeCode())
                .append(getAttributeValueCode(), other.getAttributeValueCode()).build();
    }
}
