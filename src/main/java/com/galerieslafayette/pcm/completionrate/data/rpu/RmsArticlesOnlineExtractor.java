package com.galerieslafayette.pcm.completionrate.data.rpu;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.stereotype.Component;

/**
 * Extraction <b>depuis la base RPU</b> des UG articles ayant le flag <i>OnLine GL</i> activé.
 *
 * @author p_nlegriel
 */
@Component
public class RmsArticlesOnlineExtractor {

    private static final String SEASON_CODES = "seasonCodes";

    private static final String UG_COLUMN = "UG";

    /**
     * Requête SQL retrouvant tous les UG d'articles ayant le flag <i>OnLine GL</i> activé.
     */
    public static final String UG_OF_RMS_ARTICLES_ON_LINE_REQUEST = //
            "-- Obtain all UGs of RMS articles which \"OnLine GL\" flag is activated \r\n" //
                    + "select RMSA.P_UG as " + UG_COLUMN + " from RMSARTICLES RMSA \r\n" //
                    + "  join ENUMERATIONVALUES E on E.PK = RMSA.P_SEASON and E.CODE in (:" + SEASON_CODES + ") \r\n" //
                    + "where P_ONLINE = 1";

    private final NamedParameterJdbcOperations rpuJdbcTemplate;

    @Autowired
    public RmsArticlesOnlineExtractor(NamedParameterJdbcOperations rpuJdbcTemplate) {
        this.rpuJdbcTemplate = rpuJdbcTemplate;
    }

    /**
     * Recherche toutes les lignes de la table {@code RMSARTICLES} dont la colonne {@code P_ONLINE} vaut 1.
     * 
     * @param seasonCodesFilter
     *            liste des codes saison sur laquelle filtrer la recherche
     * 
     * @return un ensemlbe jamais {@code null} contenant des UG d'articles
     */
    public Set<String> getAllOnlineGlRmsArticlesUg(Set<String> seasonCodesFilter) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(SEASON_CODES, seasonCodesFilter);

        List<String> result = rpuJdbcTemplate.query(UG_OF_RMS_ARTICLES_ON_LINE_REQUEST, parameters, new RowMapper<String>() {

            @Override
            public String mapRow(ResultSet resultSet, int rowNumber) throws SQLException {
                return resultSet.getString(UG_COLUMN);
            }
        });
        return new HashSet<>(result);
    }
}
