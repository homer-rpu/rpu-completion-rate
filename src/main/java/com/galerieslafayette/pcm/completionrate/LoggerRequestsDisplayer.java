package com.galerieslafayette.pcm.completionrate;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Affichage dans les logs des requêtes SQL exécutées par l'application.
 *
 * @author p_nlegriel
 */
@Component
public class LoggerRequestsDisplayer implements RequestsDisplayer {

    private static final Logger LOG = LoggerFactory.getLogger(LoggerRequestsDisplayer.class);

    private final RunOptions runOptions;

    @Autowired
    public LoggerRequestsDisplayer(RunOptions runOptions) {
        this.runOptions = runOptions;
    }

    @Override
    public void displayAllRequests(List<String> requests) {
        if (runOptions.isLogRequestsEnabled()) {
            LOG.info("Displaying of application requests enabled");
            for (int i = 0; i < requests.size(); i++) {
                LOG.info("Request {}:\n\n{};\n", i + 1, requests.get(i));
            }
        } else {
            LOG.debug("Displaying of application requests disabled");
        }
    }
}
