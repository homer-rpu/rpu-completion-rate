package com.galerieslafayette.pcm.completionrate;

import java.util.Set;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Regroupement des options de la ligne de commande ayant lancé l'application qui vont "filtrer" les résultats du traitement.
 * 
 * @author p_nlegriel
 */
@Component
@ConfigurationProperties
public class RunOptions {

    @Value("${rpu.completionrate.output.directory}")
    private String outputDirectory;

    @Value("${rpu.completionrate.output.base.file.name}")
    private String outputBaseFileName;

    @Value("${rpu.completionrate.output.encoding}")
    private String outputEncoding;

    @Value("${rpu.completionrate.logRequests}")
    private boolean logRequestsEnabled;

    @Value("#{'${rpu.completionrate.attributes}'.split(',')}")
    private Set<String> eligibleClassificationAttributes;

    @Value("#{'${rpu.completionrate.seasons}'.split(',')}")
    private Set<String> seasonCodesFilter;

    @Value("${rpu.project.version}")
    private String projectVersion;

    @Value("${rpu.environment.name}")
    private String environmementName;

    public String getOutputDirectory() {
        return outputDirectory;
    }

    public String getOutputBaseFileName() {
        return outputBaseFileName;
    }

    public String getOutputEncoding() {
        return outputEncoding;
    }

    public boolean isLogRequestsEnabled() {
        return logRequestsEnabled;
    }

    public Set<String> getEligibleClassificationAttributes() {
        return eligibleClassificationAttributes;
    }

    public Set<String> getSeasonCodesFilter() {
        return seasonCodesFilter;
    }

    public String getProjectVersion() {
        return projectVersion;
    }

    public String getEnvironmentName() {
        return environmementName;
    }
}
