package com.galerieslafayette.pcm.completionrate;

import static java.time.Instant.now;

import java.time.Instant;

/**
 * Interface à utiliser pour offrir un moyen de tester unitairement des comportements de classes dépendant de la date ou de l'heure courante.
 * 
 * @author nlegriel
 */
public interface Clock {

    /**
     * Fournit l'instant courant (qui peut être "surchargé" pour les besoins d'un test unitaire).
     */
    default Instant getCurrentInstant() {
        return now();
    }
}
