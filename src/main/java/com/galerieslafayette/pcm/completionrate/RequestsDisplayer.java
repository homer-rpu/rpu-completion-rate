package com.galerieslafayette.pcm.completionrate;

import java.util.List;

/**
 * Contrat des composants récupérant le détail des requêtes SQL exécutées par l'application.
 *
 * @author p_nlegriel
 */
public interface RequestsDisplayer {

    /**
     * Envoie vers une sortie le détail des requêtes fournies.
     * 
     * @param requests
     *            les requêtes à récupérer
     */
    void displayAllRequests(List<String> requests);
}
