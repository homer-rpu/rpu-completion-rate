package com.galerieslafayette.pcm.completionrate;

import static com.galerieslafayette.pcm.completionrate.data.ecom.ClassificationCategoryAndAttributeValuesForAttributeRowMapper.CLASSIFICATIONS_AND_ATTRIBUTE_VALUES_FOR_ATTRIBUTE_REQUEST;
import static com.galerieslafayette.pcm.completionrate.data.ecom.ClassificationStructureExtractor.PCM_STRUCTURE_FOR_PSSF_REQUEST;
import static com.galerieslafayette.pcm.completionrate.data.ecom.MoCoValuationDetailsExtractor.VALUES_BY_MOCO_BY_ATTRIBUTE_BY_PSSF_REQUEST;
import static com.galerieslafayette.pcm.completionrate.data.ecom.ProductAndMoCoNumbersForPssfExtractor.MOCOS_NUMER_FOR_PSSF_REQUEST;
import static com.galerieslafayette.pcm.completionrate.data.ecom.ProductAndMoCoNumbersForPssfExtractor.PRODUCTS_NUMER_FOR_PSSF_REQUEST;
import static com.galerieslafayette.pcm.completionrate.data.ecom.PssfsFromAttributeExtractor.SUB_CATEGORIES_REQUEST;
import static com.galerieslafayette.pcm.completionrate.data.ecom.ValuatedAttributesInPssfExtractor.VALUES_BY_ATTRIBUTE_BY_PSSF_REQUEST;
import static com.galerieslafayette.pcm.completionrate.data.rpu.RmsArticlesOnlineExtractor.UG_OF_RMS_ARTICLES_ON_LINE_REQUEST;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.unmodifiableList;
import static java.util.Comparator.comparing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.galerieslafayette.pcm.completionrate.data.AttributeAndValueDto;
import com.galerieslafayette.pcm.completionrate.data.CategoryStructureDto;
import com.galerieslafayette.pcm.completionrate.data.RateStructureDto;
import com.galerieslafayette.pcm.completionrate.data.ecom.ArticleDetailsDto;
import com.galerieslafayette.pcm.completionrate.data.ecom.ArticleDetailsWithFlagOnLineDto;
import com.galerieslafayette.pcm.completionrate.data.ecom.ClassificationStructureExtractor;
import com.galerieslafayette.pcm.completionrate.data.ecom.MoCoValuationDetailsExtractor;
import com.galerieslafayette.pcm.completionrate.data.ecom.ProductAndMoCoNumbersForPssfExtractor;
import com.galerieslafayette.pcm.completionrate.data.ecom.PssfsFromAttributeExtractor;
import com.galerieslafayette.pcm.completionrate.data.ecom.ValuatedAttributesInPssfExtractor;
import com.galerieslafayette.pcm.completionrate.data.rpu.RmsArticlesOnlineExtractor;
import com.galerieslafayette.pcm.completionrate.export.ResultExporter;

/**
 * Contient uniquement la logique de calcul des taux de complétion.
 *
 * @author p_nlegriel
 */
@Component
public class CompletionRateComputer {

    private static final Logger LOG = LoggerFactory.getLogger(CompletionRateComputer.class);

    /**
     * Intitulés des colonnes devant aparaître dans la sortie des données générales sur les attributs et leurs valorisation.
     */
    private static final List<String> OUTPUT_HEADERS = unmodifiableList(asList("Libellé attribut de Classification", "Code attribut de Classification",
            "Libellé Valeur de l'attribut de Classification", "Code valeur de Classification", "Univers PCM", "Famille PCM", "Sous-famille PCM",
            "Sous-sous-famille PCM", "OnLine GL", "Nombre de Mocos mappés", "Nombre de MoCos saisis", "Taux de saisie MoCos", "Nombre de produits RMS mappés",
            "Nombre de produits saisis", "Taux de saisie produits"));

    private final RunOptions runOptions;
    private final ResultExporter resultExporter;
    private final RequestsDisplayer requestsDisplayer;
    private final RmsArticlesOnlineExtractor rmsArticlesOnlineExtractor;
    private final PssfsFromAttributeExtractor pssfsFromAttributeExtractor;
    private final ProductAndMoCoNumbersForPssfExtractor productAndMoCoNumbersForPssfExtractor;
    private final ClassificationStructureExtractor classificationStructureExtractor;
    private final ValuatedAttributesInPssfExtractor valuatedAttributesInPssfExtractor;
    private final MoCoValuationDetailsExtractor moCoValuationDetailsExtractor;

    @Autowired
    public CompletionRateComputer(RunOptions runOptions, ResultExporter resultExporter, RequestsDisplayer requestsDisplayer,
            RmsArticlesOnlineExtractor rmsArticlesOnlineExtractor, PssfsFromAttributeExtractor pssfsFromAttributeExtractor,
            ProductAndMoCoNumbersForPssfExtractor productAndMoCoNumbersForPssfExtractor, ClassificationStructureExtractor classificationStructureExtractor,
            ValuatedAttributesInPssfExtractor valuatedAttributesInPssfExtractor, MoCoValuationDetailsExtractor moCoValuationDetailsExtractor) {
        this.runOptions = runOptions;
        this.resultExporter = resultExporter;
        this.requestsDisplayer = requestsDisplayer;
        this.rmsArticlesOnlineExtractor = rmsArticlesOnlineExtractor;
        this.pssfsFromAttributeExtractor = pssfsFromAttributeExtractor;
        this.productAndMoCoNumbersForPssfExtractor = productAndMoCoNumbersForPssfExtractor;
        this.classificationStructureExtractor = classificationStructureExtractor;
        this.valuatedAttributesInPssfExtractor = valuatedAttributesInPssfExtractor;
        this.moCoValuationDetailsExtractor = moCoValuationDetailsExtractor;
    }

    /**
     * Effectue le calcul des taux de complétion en fonction des arguments fournis et envoie les résultats vers le support indiqué en configuration.
     *
     * @throws IOException
     *             en cas d'échec d'export des résultats vers la sortie
     * @see ResultExporter
     */
    public void createCompletionRateFile() throws IOException {
        LOG.info("RPU-COMPLETION-RATE (find elements to compute classification attribute completion rate). Version: {}", runOptions.getProjectVersion());
        LOG.info("Current execution environment: {}", runOptions.getEnvironmentName());

        requestsDisplayer.displayAllRequests(asList(UG_OF_RMS_ARTICLES_ON_LINE_REQUEST, CLASSIFICATIONS_AND_ATTRIBUTE_VALUES_FOR_ATTRIBUTE_REQUEST,
                SUB_CATEGORIES_REQUEST, PRODUCTS_NUMER_FOR_PSSF_REQUEST, MOCOS_NUMER_FOR_PSSF_REQUEST, PCM_STRUCTURE_FOR_PSSF_REQUEST,
                VALUES_BY_ATTRIBUTE_BY_PSSF_REQUEST, VALUES_BY_MOCO_BY_ATTRIBUTE_BY_PSSF_REQUEST));

        Set<String> onLineUgArticles = rmsArticlesOnlineExtractor.getAllOnlineGlRmsArticlesUg(runOptions.getSeasonCodesFilter());

        LOG.debug("Number of articles \"OnLine GL\": {}", onLineUgArticles.size());

        SortedSet<RateStructureDto> rates = new TreeSet<>(comparing(RateStructureDto::getUniverse).thenComparing(RateStructureDto::getPf)
                .thenComparing(RateStructureDto::getPsf).thenComparing(RateStructureDto::getPssf).thenComparing(RateStructureDto::getAttributeCode)
                .thenComparing(RateStructureDto::getAttributeValueCode));

        for (String attributeCode : runOptions.getEligibleClassificationAttributes()) {
            computeRatesForAttribute(rates, attributeCode, onLineUgArticles);
        }

        writeResults(resultExporter, rates);
    }

    private void computeRatesForAttribute(SortedSet<RateStructureDto> rates, String attributeCode, Set<String> onLineUgArticles) {
        LOG.info("Extracting all PSSFs for attribute code '{}'...", attributeCode);
        Map<String, Set<AttributeAndValueDto>> pssfs = pssfsFromAttributeExtractor.extractPssfsAndAttributeValues(attributeCode);
        LOG.info("PSSFs of attribute {}: {}", attributeCode, pssfs.keySet());

        Map<String, CategoryStructureDto> pssfStructures = new HashMap<>();
        for (Entry<String, Set<AttributeAndValueDto>> pssfEntry : pssfs.entrySet()) {
            computeRatesForAttributeAndPssf(rates, attributeCode, pssfEntry, pssfStructures, onLineUgArticles);
        }
    }

    private void computeRatesForAttributeAndPssf(SortedSet<RateStructureDto> rates, String attributeCode, Entry<String, Set<AttributeAndValueDto>> pssfEntry,
            Map<String, CategoryStructureDto> pssfStructures, Set<String> onLineUgArticles) {
        int productsNumber = productAndMoCoNumbersForPssfExtractor.extractProductsNumber(pssfEntry.getKey(), runOptions.getSeasonCodesFilter());
        int mocosNumber = productAndMoCoNumbersForPssfExtractor.extractMoCosNumber(pssfEntry.getKey(), runOptions.getSeasonCodesFilter());
        CategoryStructureDto pssfHierarchy = pssfStructures.computeIfAbsent(pssfEntry.getKey(), classificationStructureExtractor::extractPcmStructureForPssf);

        LOG.debug("Found {} products and {} MoCos for PSSF '{}' ({})", productsNumber, mocosNumber, pssfEntry.getKey(), pssfHierarchy);

        List<AttributeAndValueDto> valuationsForAttributeAndPssf = valuatedAttributesInPssfExtractor.extractValuesByAttributeForPssf(pssfEntry.getKey(),
                attributeCode, runOptions.getSeasonCodesFilter());

        LOG.debug("Number of valuated products for attribute {} and PSSF {}: {}", attributeCode, pssfEntry.getKey(), valuationsForAttributeAndPssf.size());

        Map<String, Integer> valuationsByAttributeValuesByProducts = new HashMap<>();
        for (AttributeAndValueDto attributeAndValueDto : valuationsForAttributeAndPssf) {
            valuationsByAttributeValuesByProducts.computeIfAbsent(attributeAndValueDto.getAttributeValueCode(), k -> 0);
            valuationsByAttributeValuesByProducts.computeIfPresent(attributeAndValueDto.getAttributeValueCode(), (k, v) -> v + 1);
        }

        List<ArticleDetailsWithFlagOnLineDto> articleDetailsForAttributeAndPssf = extractArticleDetailsWithAttributeValuesAndOnLineFlagForPssfAndAttribue(
                pssfEntry.getKey(), attributeCode, onLineUgArticles);

        LOG.debug("Number of valuated articles for attribut {} and PSSF {}: {}", attributeCode, pssfEntry.getKey(), articleDetailsForAttributeAndPssf.size());

        Set<AttributeAndValueDto> onLineGlFlagsActivated = new HashSet<>();
        // La clé de cette map est un code de valeur d'attribut et sa valeur un ensemble d'identifiants de MoCos de la forme <ugProduuit>-<pkCouleur>
        Map<String, Set<String>> valuationsByAttributeValuesByMoCos = new HashMap<>();
        for (ArticleDetailsDto article : articleDetailsForAttributeAndPssf) {
            if (onLineUgArticles.contains(article.getUgArticle())) {
                onLineGlFlagsActivated.add(article.getAttributeAndValue());
            }

            valuationsByAttributeValuesByMoCos.computeIfAbsent(article.getAttributeAndValue().getAttributeValueCode(), k -> new HashSet<>());
            valuationsByAttributeValuesByMoCos.computeIfPresent(article.getAttributeAndValue().getAttributeValueCode(), (k, v) -> {
                v.add(getMoCoId(article));
                return v;
            });
        }

        for (AttributeAndValueDto expectedAttributeAndValueDto : pssfEntry.getValue()) {
            if (valuationsForAttributeAndPssf.contains(expectedAttributeAndValueDto)) {
                rates.add(new RateStructureDto(pssfHierarchy, expectedAttributeAndValueDto, onLineGlFlagsActivated.contains(expectedAttributeAndValueDto),
                        productsNumber, mocosNumber, valuationsByAttributeValuesByProducts.get(expectedAttributeAndValueDto.getAttributeValueCode()),
                        valuationsByAttributeValuesByMoCos.get(expectedAttributeAndValueDto.getAttributeValueCode()).size()));
            } else {
                rates.add(new RateStructureDto(pssfHierarchy, expectedAttributeAndValueDto, onLineGlFlagsActivated.contains(expectedAttributeAndValueDto),
                        productsNumber, mocosNumber, 0, 0));
            }
        }
    }

    private List<ArticleDetailsWithFlagOnLineDto> extractArticleDetailsWithAttributeValuesAndOnLineFlagForPssfAndAttribue(String pssf, String attributeCode,
            Set<String> onLineUgArticles) {
        List<ArticleDetailsDto> articleDetailsForAttributeAndPssf = moCoValuationDetailsExtractor
                .extractArticleDetailsWithAttributeValuesForPssfAndAttribue(pssf, attributeCode, runOptions.getSeasonCodesFilter());

        List<ArticleDetailsWithFlagOnLineDto> articleDetailsWithFlagOnLine = new ArrayList<>();
        for (ArticleDetailsDto articleDetails : articleDetailsForAttributeAndPssf) {
            articleDetailsWithFlagOnLine.add(new ArticleDetailsWithFlagOnLineDto(articleDetails, onLineUgArticles.contains(articleDetails.getUgArticle())));
        }

        return articleDetailsWithFlagOnLine;
    }

    private String getMoCoId(ArticleDetailsDto article) {
        return new StringBuilder(article.getUgProduct()).append('-').append(article.getColorPk()).toString();
    }

    private void writeResults(ResultExporter resultExporter, SortedSet<RateStructureDto> rates) throws IOException {
        resultExporter.init();
        resultExporter.writeHeader(OUTPUT_HEADERS);

        for (RateStructureDto rateStructure : rates) {
            resultExporter.writeRow(asList(rateStructure.getAttributeName(), rateStructure.getAttributeCode(), rateStructure.getAttributeValueName(),
                    rateStructure.getAttributeValueCode(), rateStructure.getUniverseLabel(), rateStructure.getPfLabel(), rateStructure.getPsfLabel(),
                    rateStructure.getPssfLabel(), writeOnLineGlFlag(rateStructure), rateStructure.getMappedMoCosNumber(),
                    rateStructure.getValuatedMoCosNumber(), rateStructure.getValuatedMocosRate(), rateStructure.getMappedProductsNumber(),
                    rateStructure.getValuatedProductsNumber(), rateStructure.getValuatedProductsRate()));
        }

        resultExporter.writeRow(emptyList());
        resultExporter.close();
    }

    private String writeOnLineGlFlag(RateStructureDto rateStructure) {
        if (rateStructure.isOnLineGl()) {
            return "Oui";
        } else {
            return "Non";
        }
    }
}
