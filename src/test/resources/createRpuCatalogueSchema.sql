set database sql syntax ora true;

create table if not exists RMSARTICLES (
	PK number(20, 0), 
	P_UG varchar2(255 char), 
	P_NAME varchar2(255 char), 
	P_EAN varchar2(255 char), 
	P_SEASON number(20, 0), 
	P_ONLINE number(1, 0), 
	P_PRODUCT number(20, 0), 
	primary key (PK)
);

create table if not exists ENUMERATIONVALUES (	
	PK number(20, 0), 
	CODE varchar2(255 char), 
	primary key (PK)
);
