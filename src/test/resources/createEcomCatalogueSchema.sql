set database sql syntax ora true;

create table if not exists CATALOGS ( 
	PK number(20, 0),
	primary key (PK)
);

create table if not exists CATALOGVERSIONS (
	PK number(20, 0), 
	P_VERSION varchar2(255), 
	P_CATALOG number(20, 0),
	primary key (PK)
);

create table if not exists ENUMERATIONVALUES (	
	PK number(20, 0), 
	CODE varchar2(255), 
	primary key (PK)
);

create table if not exists CATEGORIES (
	PK number(20, 0), 
	P_CODE varchar2(255), 
	P_CATALOG number(20, 0), 
	P_CATALOGVERSION number(20, 0), 
	primary key (PK)
);

create table if not exists CAT2CATREL ( 
	PK number(20, 0), 
	SOURCEPK number(20, 0), 
	TARGETPK number(20, 0), 
	primary key (PK)
);

create table if not exists CLASSIFICATIONATTRS (
	PK number(20, 0), 
	P_SYSTEMVERSION number(20, 0), 
	P_CODE varchar2(255), 
	primary key (PK)
);

create table if not exists CLASSIFICATIONATTRSLP (
	ITEMPK number(20, 0), 
	LANGPK number(20, 0), 
	P_NAME varchar2(255), 
	primary key (ITEMPK, LANGPK)
);

create table if not exists CAT2ATTRREL (
	PK number(20, 0),
	P_CLASSIFICATIONCLASS number(20, 0), 
	P_CLASSIFICATIONATTRIBUTE number(20, 0), 
	primary key (PK)
);

create table if not exists CLASSATTRVALUES (
	PK number(20, 0), 
	P_CODE varchar2(255), 
	P_SYSTEMVERSION number(20, 0), 
	primary key (PK)
);

create table if not exists CLASSATTRVALUESLP ( 
   	ITEMPK number(20, 0), 
	LANGPK number(20, 0), 
	P_NAME varchar2(255), 
	primary key (ITEMPK, LANGPK)
);

create table if not exists LANGUAGES (
	PK number(20, 0), 
	P_ISOCODE varchar2(255), 
	primary key (PK)
);

create table if not exists PRODUCTS (
	PK number(20, 0), 
	CODE varchar2(255), 
	P_CATALOGVERSION number(20, 0), 
	P_CATALOG number(20, 0), 
	P_BASEPRODUCT number(20, 0), 
	P_SEASON number(20, 0), 
	P_PRODUCTWORKFLOWSTATUS number(20, 0), 
	P_ARTICLEWORKFLOWSTATUS number(20, 0), 
	P_COLOR number(20, 0), 
	primary key (PK)
);
