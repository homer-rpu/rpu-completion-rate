package com.galerieslafayette.pcm.completionrate;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.galerieslafayette.pcm.completionrate.export.ResultExporter;

@RunWith(SpringRunner.class)
@JdbcTest
@ComponentScan(excludeFilters = { @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = Launcher.class) })
@ContextConfiguration(classes = TestConfiguration.class)
public class CompletionRateComputerIT {

    @MockBean
    private RunOptions runOptions;

    @MockBean
    private ResultExporter resultExporter;

    @Autowired
    private CompletionRateComputer completionRateComputer;

    @Before
    public void setUp() {
        given(runOptions.getOutputDirectory()).willReturn(Paths.get("target", "output-test").toString());
        given(runOptions.getSeasonCodesFilter()).willReturn(new HashSet<>(asList("116")));
    }

    @Test
    public void whenDatabaseIsEmptyResultShouldBeEmpty() throws IOException {
        completionRateComputer.createCompletionRateFile();
        verify(resultExporter).writeRow(eq(emptyList()));
    }

    @Test
    public void whenOnePSSFOneAttributeOneValueOneProductAndOneAttributeMappedToProduct_twoLinesShouldBeExportedWith0PercentRate() {
        // TODO
    }
}
