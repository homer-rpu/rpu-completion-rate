package com.galerieslafayette.pcm.completionrate;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

public class TestConfiguration {

    // private final DataSource dataSource = new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.HSQL).addScript("createRpuCatalogueSchema.sql").build();

    @Bean
    @Primary
    public DataSource ecomDataSource() {
        // return dataSource;
        return new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.HSQL).addScript("createEcomCatalogueSchema.sql").build();
    }

    @Bean
    public DataSource rpuDataSource() {
        // return dataSource;
        return new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.HSQL).addScript("createRpuCatalogueSchema.sql").build();
    }

    @Bean
    public NamedParameterJdbcOperations ecomJdbcTemplate() {
        return new NamedParameterJdbcTemplate(ecomDataSource());
    }

    @Bean
    public NamedParameterJdbcOperations rpuJdbcTemplate() {
        return new NamedParameterJdbcTemplate(rpuDataSource());
    }
}
