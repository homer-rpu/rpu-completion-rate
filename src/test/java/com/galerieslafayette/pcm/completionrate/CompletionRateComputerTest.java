package com.galerieslafayette.pcm.completionrate;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;
import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.map.HashedMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.galerieslafayette.pcm.completionrate.data.AttributeAndValueDto;
import com.galerieslafayette.pcm.completionrate.data.CategoryStructureDto;
import com.galerieslafayette.pcm.completionrate.data.ecom.ArticleDetailsDto;
import com.galerieslafayette.pcm.completionrate.data.ecom.ClassificationStructureExtractor;
import com.galerieslafayette.pcm.completionrate.data.ecom.MoCoValuationDetailsExtractor;
import com.galerieslafayette.pcm.completionrate.data.ecom.ProductAndMoCoNumbersForPssfExtractor;
import com.galerieslafayette.pcm.completionrate.data.ecom.PssfsFromAttributeExtractor;
import com.galerieslafayette.pcm.completionrate.data.ecom.ValuatedAttributesInPssfExtractor;
import com.galerieslafayette.pcm.completionrate.data.rpu.RmsArticlesOnlineExtractor;
import com.galerieslafayette.pcm.completionrate.export.ResultExporter;

@RunWith(MockitoJUnitRunner.class)
public class CompletionRateComputerTest {

    // Jeu de données pour une PSSF en base
    private static final String universeCode = "PU";
    private static final String universeName = "Nom de l'univers";
    private static final String pfCode = "PF";
    private static final String pfName = "Nom de la famille";
    private static final String psfCode = "PSF";
    private static final String psfName = "Nom de la sous-famille";
    private static final String pssfCode = "PSSF";
    private static final String pssfName = "Nom de la sous-sous-famille";

    // Jeu de données pour deux PSSF appartenant chacune à une arborescence de catégorie en base
    private static final String universeCode1 = "PU_1";
    private static final String universeName1 = "Nom de l'univers 1";
    private static final String pfCode1 = "PF_1";
    private static final String pfName1 = "Nom de la famille 1";
    private static final String psfCode1 = "PSF_1";
    private static final String psfName1 = "Nom de la sous-famille 1";
    private static final String pssfCode1 = "PSSF_1";
    private static final String pssfName1 = "Nom de la sous-sous-famille 1";

    private static final String universeCode2 = "PU_2";
    private static final String universeName2 = "Nom de l'univers 2";
    private static final String pfCode2 = "PF_2";
    private static final String pfName2 = "Nom de la famille 2";
    private static final String psfCode2 = "PSF_2";
    private static final String psfName2 = "Nom de la sous-famille 2";
    private static final String pssfCode2 = "PSSF_2";
    private static final String pssfName2 = "Nom de la sous-sous-famille 2";

    @Mock
    private RmsArticlesOnlineExtractor rmsArticlesOnlineExtractor;
    @Mock
    private PssfsFromAttributeExtractor pssfsFromAttributeExtractor;
    @Mock
    private ProductAndMoCoNumbersForPssfExtractor productAndMoCoNumbersForPssfExtractor;
    @Mock
    private ClassificationStructureExtractor classificationStructureExtractor;
    @Mock
    private ValuatedAttributesInPssfExtractor valuatedAttributesInPssfExtractor;
    @Mock
    private MoCoValuationDetailsExtractor moCoValuationDetailsExtractor;
    @Mock
    private RunOptions runOptions;
    @Mock
    private ResultExporter resultExporter;
    @Mock
    private RequestsDisplayer requestsDisplayer;

    @Captor
    private ArgumentCaptor<List<String>> rowDataCapture;
    @Captor
    private ArgumentCaptor<List<String>> rowRatesCapture;

    /** Component under test. */
    private CompletionRateComputer completionRateComputer;

    @Before
    public void setUp() {
        completionRateComputer = new CompletionRateComputer(runOptions, resultExporter, requestsDisplayer, rmsArticlesOnlineExtractor,
                pssfsFromAttributeExtractor, productAndMoCoNumbersForPssfExtractor, classificationStructureExtractor, valuatedAttributesInPssfExtractor,
                moCoValuationDetailsExtractor);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void showRequest() throws IOException {
        // When
        completionRateComputer.createCompletionRateFile();

        // Then
        @SuppressWarnings("rawtypes")
        ArgumentCaptor<List> requestsCapture = ArgumentCaptor.forClass(List.class);

        verify(requestsDisplayer).displayAllRequests(requestsCapture.capture());

        assertThat(requestsCapture.getValue()).hasSize(8).allMatch(s -> s.toString().contains("select "));
        assertThat(requestsCapture.getValue().get(0).toString()).contains("RMS");
        assertThat(requestsCapture.getValue().get(1).toString()).endsWith("?");
        assertThat(requestsCapture.getValue().get(2).toString()).endsWith("?");
        assertThat(requestsCapture.getValue().get(3).toString()).endsWith(":pssfCode");
        assertThat(requestsCapture.getValue().get(4).toString()).endsWith(":pssfCode)");
        assertThat(requestsCapture.getValue().get(5).toString()).endsWith("?");
        assertThat(requestsCapture.getValue().get(6).toString()).contains(":attributeCode");
        assertThat(requestsCapture.getValue().get(7).toString()).endsWith(":attributeCode");
    }

    @Test
    public void createCompletionRateFileWhenOnePSSFOneAttributeOneValueOneProductAndOneAttributeMappedToProduct_twoLinesShouldBeExportedWith0PercentRate()
            throws IOException {
        // Given
        final String attributeCode = "ATTR";
        when(runOptions.getEligibleClassificationAttributes()).thenReturn(singleton(attributeCode));
        final CategoryStructureDto category = new CategoryStructureDto(universeCode, universeName, pfCode, pfName, psfCode, psfName, pssfCode, pssfName);

        final AttributeAndValueDto attributeAndValue = new AttributeAndValueDto(attributeCode, "Libellé d'attribut", "ATTR_VALUE",
                "Nom de la valeur d'attribut");

        final Map<String, Set<AttributeAndValueDto>> pssfWithValues = new HashedMap<>();
        pssfWithValues.put(pssfCode, new HashSet<>());
        pssfWithValues.get(pssfCode).add(attributeAndValue);

        when(rmsArticlesOnlineExtractor.getAllOnlineGlRmsArticlesUg(any())).thenReturn(emptySet());
        when(pssfsFromAttributeExtractor.extractPssfsAndAttributeValues(anyString())).thenReturn(pssfWithValues);
        when(productAndMoCoNumbersForPssfExtractor.extractProductsNumber(anyString(), any())).thenReturn(1);
        when(productAndMoCoNumbersForPssfExtractor.extractMoCosNumber(anyString(), any())).thenReturn(1);
        when(classificationStructureExtractor.extractPcmStructureForPssf(anyString())).thenReturn(category);
        // Enjeu du test : pas de produits, ni de MoCos valorisés
        when(valuatedAttributesInPssfExtractor.extractValuesByAttributeForPssf(anyString(), anyString(), any())).thenReturn(emptyList());
        when(moCoValuationDetailsExtractor.extractArticleDetailsWithAttributeValuesForPssfAndAttribue(anyString(), anyString(), any())).thenReturn(emptyList());

        // When
        completionRateComputer.createCompletionRateFile();

        // Then
        assertRatesStructure(asList(asList(attributeAndValue.getAttributeName(), attributeAndValue.getAttributeCode(),
                attributeAndValue.getAttributeValueName(), attributeAndValue.getAttributeValueCode(), "PU - Nom de l'univers", "PF - Nom de la famille",
                "PSF - Nom de la sous-famille", "PSSF - Nom de la sous-sous-famille", "Non", "1", "0", "0.0 %", "1", "0", "0.0 %"), emptyList()));
        // TODO activer la séparation par attribut GL Online
        // assertRatesStructure(asList(
        // asList(attributeAndValue.getAttributeName(), attributeAndValue.getAttributeCode(), attributeAndValue.getAttributeValueName(),
        // attributeAndValue.getAttributeValueCode(), "PU - Nom de l'univers", "PF - Nom de la famille", "PSF - Nom de la sous-famille",
        // "PSSF - Nom de la sous-sous-famille", "Non", "1", "0", "0.0 %", "1", "0", "0.0 %"),
        // asList(attributeAndValue.getAttributeName(), attributeAndValue.getAttributeCode(), attributeAndValue.getAttributeValueName(),
        // attributeAndValue.getAttributeValueCode(), "PU - Nom de l'univers", "PF - Nom de la famille", "PSF - Nom de la sous-famille",
        // "PSSF - Nom de la sous-sous-famille", "Oui", "1", "0", "0.0 %", "1", "0", "0.0 %"),
        // emptyList()));
    }

    @Test
    public void createCompletionRateFileWhenOnePSSFOneAttributeOneValueOneProductAndAttributeMappedWithValueToThisProduct_twoLinesShouldBeExportedWith100PercentRates()
            throws IOException {
        // Given
        final String attributeCode = "ATTR";
        when(runOptions.getEligibleClassificationAttributes()).thenReturn(singleton(attributeCode));
        final CategoryStructureDto category = new CategoryStructureDto(universeCode, universeName, pfCode, pfName, psfCode, psfName, pssfCode, pssfName);

        final AttributeAndValueDto attributeAndValue = new AttributeAndValueDto(attributeCode, "Libellé d'attribut", "ATTR_VALUE",
                "Nom de la valeur d'attribut");

        final Map<String, Set<AttributeAndValueDto>> pssfWithValues = new HashedMap<>();
        pssfWithValues.put(pssfCode, new HashSet<>());
        pssfWithValues.get(pssfCode).add(attributeAndValue);

        when(rmsArticlesOnlineExtractor.getAllOnlineGlRmsArticlesUg(any())).thenReturn(emptySet());
        when(pssfsFromAttributeExtractor.extractPssfsAndAttributeValues(any())).thenReturn(pssfWithValues);
        when(productAndMoCoNumbersForPssfExtractor.extractProductsNumber(anyString(), any())).thenReturn(1);
        when(productAndMoCoNumbersForPssfExtractor.extractMoCosNumber(anyString(), any())).thenReturn(1);
        when(classificationStructureExtractor.extractPcmStructureForPssf(anyString())).thenReturn(category);
        // Enjeu du test : le produit (et donc son seul MoCo) a son attribut valorisé
        when(valuatedAttributesInPssfExtractor.extractValuesByAttributeForPssf(anyString(), anyString(), any())).thenReturn(singletonList(attributeAndValue));
        when(moCoValuationDetailsExtractor.extractArticleDetailsWithAttributeValuesForPssfAndAttribue(anyString(), anyString(), any()))
                .thenReturn(singletonList(new ArticleDetailsDto(attributeAndValue, "111", "101010", "100100")));

        // When
        completionRateComputer.createCompletionRateFile();

        // Then
        assertRatesStructure(asList(asList(attributeAndValue.getAttributeName(), attributeAndValue.getAttributeCode(),
                attributeAndValue.getAttributeValueName(), attributeAndValue.getAttributeValueCode(), "PU - Nom de l'univers", "PF - Nom de la famille",
                "PSF - Nom de la sous-famille", "PSSF - Nom de la sous-sous-famille", "Non", "1", "1", "100.0 %", "1", "1", "100.0 %"), emptyList()));
        // TODO activer la séparation par attribut GL Online
        // assertRatesStructure(asList(
        // asList(attributeAndValue.getAttributeName(), attributeAndValue.getAttributeCode(), attributeAndValue.getAttributeValueName(),
        // attributeAndValue.getAttributeValueCode(), "PU - Nom de l'univers", "PF - Nom de la famille", "PSF - Nom de la sous-famille",
        // "PSSF - Nom de la sous-sous-famille", "Non", "1", "1", "100.0 %", "1", "1", "100.0 %"),
        // asList(attributeAndValue.getAttributeName(), attributeAndValue.getAttributeCode(), attributeAndValue.getAttributeValueName(),
        // attributeAndValue.getAttributeValueCode(), "PU - Nom de l'univers", "PF - Nom de la famille", "PSF - Nom de la sous-famille",
        // "PSSF - Nom de la sous-sous-famille", "Oui", "1", "0", "0.0 %", "1", "0", "0.0 %"),
        // emptyList()));
    }

    @Test
    public void createCompletionRateFileWhenOnePSSFOneAttributeOneValueTwoProductsAndAttributeMappedToTheseProductsAndOnlyOneValue_twoLinesShouldBeExportedWith50PercentRates()
            throws IOException {
        // Given
        final String attributeCode = "ATTR";
        when(runOptions.getEligibleClassificationAttributes()).thenReturn(singleton(attributeCode));
        final CategoryStructureDto category = new CategoryStructureDto(universeCode, universeName, pfCode, pfName, psfCode, psfName, pssfCode, pssfName);

        final AttributeAndValueDto attributeAndValue = new AttributeAndValueDto(attributeCode, "Libellé d'attribut", "ATTR_VALUE",
                "Nom de la valeur d'attribut");

        final Map<String, Set<AttributeAndValueDto>> pssfWithValues = new HashedMap<>();
        pssfWithValues.put(pssfCode, new HashSet<>());
        pssfWithValues.get(pssfCode).add(attributeAndValue);

        when(rmsArticlesOnlineExtractor.getAllOnlineGlRmsArticlesUg(any())).thenReturn(emptySet());
        when(pssfsFromAttributeExtractor.extractPssfsAndAttributeValues(any())).thenReturn(pssfWithValues);
        when(productAndMoCoNumbersForPssfExtractor.extractProductsNumber(anyString(), any())).thenReturn(2);
        when(productAndMoCoNumbersForPssfExtractor.extractMoCosNumber(anyString(), any())).thenReturn(2);
        when(classificationStructureExtractor.extractPcmStructureForPssf(anyString())).thenReturn(category);
        // Enjeu du test : un seul des deux produits (donc un seul des deux MoCos) valorisé
        when(valuatedAttributesInPssfExtractor.extractValuesByAttributeForPssf(anyString(), anyString(), any())).thenReturn(singletonList(attributeAndValue));
        when(moCoValuationDetailsExtractor.extractArticleDetailsWithAttributeValuesForPssfAndAttribue(anyString(), anyString(), any()))
                .thenReturn(singletonList(new ArticleDetailsDto(attributeAndValue, "111", "101010", "100100")));

        // When
        completionRateComputer.createCompletionRateFile();

        // Then
        assertRatesStructure(asList(asList(attributeAndValue.getAttributeName(), attributeAndValue.getAttributeCode(),
                attributeAndValue.getAttributeValueName(), attributeAndValue.getAttributeValueCode(), "PU - Nom de l'univers", "PF - Nom de la famille",
                "PSF - Nom de la sous-famille", "PSSF - Nom de la sous-sous-famille", "Non", "2", "1", "50.0 %", "2", "1", "50.0 %"), emptyList()));
        // TODO activer la séparation par attribut GL Online
        // assertRatesStructure(asList(
        // asList(attributeAndValue.getAttributeName(), attributeAndValue.getAttributeCode(), attributeAndValue.getAttributeValueName(),
        // attributeAndValue.getAttributeValueCode(), "PU - Nom de l'univers", "PF - Nom de la famille", "PSF - Nom de la sous-famille",
        // "PSSF - Nom de la sous-sous-famille", "Non", "2", "1", "50.0 %", "2", "1", "50.0 %"),
        // asList(attributeAndValue.getAttributeName(), attributeAndValue.getAttributeCode(), attributeAndValue.getAttributeValueName(),
        // attributeAndValue.getAttributeValueCode(), "PU - Nom de l'univers", "PF - Nom de la famille", "PSF - Nom de la sous-famille",
        // "PSSF - Nom de la sous-sous-famille", "Oui", "2", "0", "0.0 %", "2", "0", "0.0 %"),
        // emptyList()));
    }

    @Test
    public void createCompletionRateFileWhenOnePSSFOneAttributeOneValueTwoProductsOnLineAndAttributeMappedToTheseProductsAndOnlyOneValue_twoLinesShouldBeExportedWith50PercentRatesAndOnLineGlFlagOn()
            throws IOException {
        // Given
        final String attributeCode = "ATTR";
        when(runOptions.getEligibleClassificationAttributes()).thenReturn(singleton(attributeCode));
        final CategoryStructureDto category = new CategoryStructureDto(universeCode, universeName, pfCode, pfName, psfCode, psfName, pssfCode, pssfName);

        final AttributeAndValueDto attributeAndValue = new AttributeAndValueDto(attributeCode, "Libellé d'attribut", "ATTR_VALUE",
                "Nom de la valeur d'attribut");

        final Map<String, Set<AttributeAndValueDto>> pssfWithValues = new HashedMap<>();
        pssfWithValues.put(pssfCode, new HashSet<>());
        pssfWithValues.get(pssfCode).add(attributeAndValue);

        // Enjeu du test : un article possède le flag "OnLine GL" activé
        final String ugArticle1 = "111";
        when(rmsArticlesOnlineExtractor.getAllOnlineGlRmsArticlesUg(any())).thenReturn(new HashSet<>(asList(ugArticle1)));

        when(pssfsFromAttributeExtractor.extractPssfsAndAttributeValues(anyString())).thenReturn(pssfWithValues);
        when(productAndMoCoNumbersForPssfExtractor.extractProductsNumber(anyString(), any())).thenReturn(2);
        when(productAndMoCoNumbersForPssfExtractor.extractMoCosNumber(anyString(), any())).thenReturn(2);
        when(classificationStructureExtractor.extractPcmStructureForPssf(anyString())).thenReturn(category);
        when(valuatedAttributesInPssfExtractor.extractValuesByAttributeForPssf(anyString(), anyString(), any())).thenReturn(singletonList(attributeAndValue));
        when(moCoValuationDetailsExtractor.extractArticleDetailsWithAttributeValuesForPssfAndAttribue(anyString(), anyString(), any()))
                .thenReturn(singletonList(new ArticleDetailsDto(attributeAndValue, ugArticle1, "101010", "100100")));

        // When
        completionRateComputer.createCompletionRateFile();

        // Then
        assertRatesStructure(asList(asList(attributeAndValue.getAttributeName(), attributeAndValue.getAttributeCode(),
                attributeAndValue.getAttributeValueName(), attributeAndValue.getAttributeValueCode(), "PU - Nom de l'univers", "PF - Nom de la famille",
                "PSF - Nom de la sous-famille", "PSSF - Nom de la sous-sous-famille", "Oui", "2", "1", "50.0 %", "2", "1", "50.0 %"), emptyList()));
        // TODO activer la séparation par attribut GL Online
        // asList(attributeAndValue.getAttributeName(), attributeAndValue.getAttributeCode(), attributeAndValue.getAttributeValueName(),
        // attributeAndValue.getAttributeValueCode(), "PU - Nom de l'univers", "PF - Nom de la famille", "PSF - Nom de la sous-famille",
        // "PSSF - Nom de la sous-sous-famille", "Non", "2", "0", "0.0 %", "2", "0", "0.0 %"),
        // asList(attributeAndValue.getAttributeName(), attributeAndValue.getAttributeCode(), attributeAndValue.getAttributeValueName(),
        // attributeAndValue.getAttributeValueCode(), "PU - Nom de l'univers", "PF - Nom de la famille", "PSF - Nom de la sous-famille",
        // "PSSF - Nom de la sous-sous-famille", "Oui", "2", "1", "50.0 %", "2", "1", "50.0 %"),
        // emptyList()));
    }

    @Test
    public void createCompletionRateFileWhenPSSFOneAttributeTwoValuesTwoProductsAndAttributeMappedToTheseProductsAndDifferentAttributeValuatedForBothProducts_fourLineShouldBeExportedWith50PercentRates()
            throws IOException {
        // Given
        final String attributeCode = "ATTR";
        when(runOptions.getEligibleClassificationAttributes()).thenReturn(singleton(attributeCode));
        final CategoryStructureDto category = new CategoryStructureDto(universeCode, universeName, pfCode, pfName, psfCode, psfName, pssfCode, pssfName);

        final AttributeAndValueDto attributeAndValue1 = new AttributeAndValueDto(attributeCode, "Libellé d'attribut", "ATTR_VALUE_1",
                "Libellé valeur d'attribut 1");
        final AttributeAndValueDto attributeAndValue2 = new AttributeAndValueDto(attributeCode, "Libellé d'attribut", "ATTR_VALUE_2",
                "Libellé valeur d'attribut 2");

        final Map<String, Set<AttributeAndValueDto>> pssfWithValuesForAttribute = new HashedMap<>();
        pssfWithValuesForAttribute.put(pssfCode, new HashSet<>());
        pssfWithValuesForAttribute.get(pssfCode).add(attributeAndValue1);
        pssfWithValuesForAttribute.get(pssfCode).add(attributeAndValue2);

        when(rmsArticlesOnlineExtractor.getAllOnlineGlRmsArticlesUg(any())).thenReturn(emptySet());
        when(pssfsFromAttributeExtractor.extractPssfsAndAttributeValues(any())).thenReturn(pssfWithValuesForAttribute);
        when(productAndMoCoNumbersForPssfExtractor.extractProductsNumber(anyString(), any())).thenReturn(2);
        when(productAndMoCoNumbersForPssfExtractor.extractMoCosNumber(anyString(), any())).thenReturn(2);
        when(classificationStructureExtractor.extractPcmStructureForPssf(anyString())).thenReturn(category);
        // Enjeu du test : les deux produits valorisés mais chacun avec une valeur différente
        when(valuatedAttributesInPssfExtractor.extractValuesByAttributeForPssf(anyString(), anyString(), any()))
                .thenReturn(asList(attributeAndValue1, attributeAndValue2));
        when(moCoValuationDetailsExtractor.extractArticleDetailsWithAttributeValuesForPssfAndAttribue(anyString(), anyString(), any())).thenReturn(asList(
                new ArticleDetailsDto(attributeAndValue1, "111", "101010", "100100"), new ArticleDetailsDto(attributeAndValue2, "222", "202020", "200200")));

        // When
        completionRateComputer.createCompletionRateFile();

        // Then
        assertRatesStructure(asList(
                asList(attributeAndValue1.getAttributeName(), attributeAndValue1.getAttributeCode(), attributeAndValue1.getAttributeValueName(),
                        attributeAndValue1.getAttributeValueCode(), "PU - Nom de l'univers", "PF - Nom de la famille", "PSF - Nom de la sous-famille",
                        "PSSF - Nom de la sous-sous-famille", "Non", "2", "1", "50.0 %", "2", "1", "50.0 %"),
                asList(attributeAndValue2.getAttributeName(), attributeAndValue2.getAttributeCode(), attributeAndValue2.getAttributeValueName(),
                        attributeAndValue2.getAttributeValueCode(), "PU - Nom de l'univers", "PF - Nom de la famille", "PSF - Nom de la sous-famille",
                        "PSSF - Nom de la sous-sous-famille", "Non", "2", "1", "50.0 %", "2", "1", "50.0 %"),
                emptyList()));
        // TODO activer la séparation par attribut GL Online
        // asList(attributeAndValue1.getAttributeName(), attributeAndValue1.getAttributeCode(), attributeAndValue1.getAttributeValueName(),
        // attributeAndValue1.getAttributeValueCode(), "PU - Nom de l'univers", "PF - Nom de la famille", "PSF - Nom de la sous-famille",
        // "PSSF - Nom de la sous-sous-famille", "Non", "2", "1", "50.0 %", "2", "1", "50.0 %"),
        // asList(attributeAndValue1.getAttributeName(), attributeAndValue1.getAttributeCode(), attributeAndValue1.getAttributeValueName(),
        // attributeAndValue1.getAttributeValueCode(), "PU - Nom de l'univers", "PF - Nom de la famille", "PSF - Nom de la sous-famille",
        // "PSSF - Nom de la sous-sous-famille", "Oui", "2", "0", "0.0 %", "2", "0", "0.0 %"),
        // asList(attributeAndValue2.getAttributeName(), attributeAndValue2.getAttributeCode(), attributeAndValue2.getAttributeValueName(),
        // attributeAndValue2.getAttributeValueCode(), "PU - Nom de l'univers", "PF - Nom de la famille", "PSF - Nom de la sous-famille",
        // "PSSF - Nom de la sous-sous-famille", "Non", "2", "1", "50.0 %", "2", "1", "50.0 %"),
        // asList(attributeAndValue2.getAttributeName(), attributeAndValue2.getAttributeCode(), attributeAndValue2.getAttributeValueName(),
        // attributeAndValue2.getAttributeValueCode(), "PU - Nom de l'univers", "PF - Nom de la famille", "PSF - Nom de la sous-famille",
        // "PSSF - Nom de la sous-sous-famille", "Oui", "2", "0", "0.0 %", "2", "0", "0.0 %"),
        // emptyList()));
    }

    @Test
    public void createCompletionRateFileWhenPSSFOneAttributeTwoValuesTwoProductsOneMoCoForOneProductAndTwoMoCosForTheOtherAndAttributeMappedToTheseProductsAndDifferentAttributeValuatedForBothProducts_fourLineShouldBeExportedWith50PercentRatesForProductsAndDifferentRatesForMoCos()
            throws IOException {
        // Given
        final String attributeCode = "ATTR";
        when(runOptions.getEligibleClassificationAttributes()).thenReturn(singleton(attributeCode));
        final CategoryStructureDto category = new CategoryStructureDto(universeCode, universeName, pfCode, pfName, psfCode, psfName, pssfCode, pssfName);

        final AttributeAndValueDto attributeAndValue1 = new AttributeAndValueDto(attributeCode, "Libellé d'attribut", "ATTR_VALUE_1",
                "Libellé valeur d'attribut 1");
        final AttributeAndValueDto attributeAndValue2 = new AttributeAndValueDto(attributeCode, "Libellé d'attribut", "ATTR_VALUE_2",
                "Libellé valeur d'attribut 2");

        final String ugProduct1 = "101010";
        final String ugProduct2 = "202020";

        final Map<String, Set<AttributeAndValueDto>> pssfWithValuesForAttribute = new HashedMap<>();
        pssfWithValuesForAttribute.put(pssfCode, new HashSet<>());
        pssfWithValuesForAttribute.get(pssfCode).add(attributeAndValue1);
        pssfWithValuesForAttribute.get(pssfCode).add(attributeAndValue2);

        when(rmsArticlesOnlineExtractor.getAllOnlineGlRmsArticlesUg(any())).thenReturn(emptySet());
        when(pssfsFromAttributeExtractor.extractPssfsAndAttributeValues(anyString())).thenReturn(pssfWithValuesForAttribute);
        when(productAndMoCoNumbersForPssfExtractor.extractProductsNumber(anyString(), any())).thenReturn(2);
        when(productAndMoCoNumbersForPssfExtractor.extractMoCosNumber(anyString(), any())).thenReturn(3);
        when(classificationStructureExtractor.extractPcmStructureForPssf(anyString())).thenReturn(category);
        when(valuatedAttributesInPssfExtractor.extractValuesByAttributeForPssf(anyString(), anyString(), any()))
                .thenReturn(asList(attributeAndValue1, attributeAndValue2));
        // Enjeu du test : structure de MoCos qui rend les taux différents entre MoCos et produits (1 MoCo pour premier produit et 2 MoCos pour l'autre produit)
        when(moCoValuationDetailsExtractor.extractArticleDetailsWithAttributeValuesForPssfAndAttribue(anyString(), anyString(), any())).thenReturn(asList(
                new ArticleDetailsDto(attributeAndValue1, "111", ugProduct1, "100100"), new ArticleDetailsDto(attributeAndValue2, "222", ugProduct2, "200200"),
                new ArticleDetailsDto(attributeAndValue2, "333", ugProduct2, "300300")));

        // When
        completionRateComputer.createCompletionRateFile();

        // Then
        assertRatesStructure(asList(
                asList(attributeAndValue1.getAttributeName(), attributeAndValue1.getAttributeCode(), attributeAndValue1.getAttributeValueName(),
                        attributeAndValue1.getAttributeValueCode(), "PU - Nom de l'univers", "PF - Nom de la famille", "PSF - Nom de la sous-famille",
                        "PSSF - Nom de la sous-sous-famille", "Non", "3", "1", "33.3 %", "2", "1", "50.0 %"),
                asList(attributeAndValue2.getAttributeName(), attributeAndValue2.getAttributeCode(), attributeAndValue2.getAttributeValueName(),
                        attributeAndValue2.getAttributeValueCode(), "PU - Nom de l'univers", "PF - Nom de la famille", "PSF - Nom de la sous-famille",
                        "PSSF - Nom de la sous-sous-famille", "Non", "3", "2", "66.7 %", "2", "1", "50.0 %"),
                emptyList()));
        // TODO activer la séparation par attribut GL Online
        // asList(attributeAndValue1.getAttributeName(), attributeAndValue1.getAttributeCode(), attributeAndValue1.getAttributeValueName(),
        // attributeAndValue1.getAttributeValueCode(), "PU - Nom de l'univers", "PF - Nom de la famille", "PSF - Nom de la sous-famille",
        // "PSSF - Nom de la sous-sous-famille", "Non", "3", "1", "33.3 %", "2", "1", "50.0 %"),
        // asList(attributeAndValue1.getAttributeName(), attributeAndValue1.getAttributeCode(), attributeAndValue1.getAttributeValueName(),
        // attributeAndValue1.getAttributeValueCode(), "PU - Nom de l'univers", "PF - Nom de la famille", "PSF - Nom de la sous-famille",
        // "PSSF - Nom de la sous-sous-famille", "Oui", "3", "0", "0.0 %", "2", "0", "0.0 %"),
        // asList(attributeAndValue2.getAttributeName(), attributeAndValue2.getAttributeCode(), attributeAndValue2.getAttributeValueName(),
        // attributeAndValue2.getAttributeValueCode(), "PU - Nom de l'univers", "PF - Nom de la famille", "PSF - Nom de la sous-famille",
        // "PSSF - Nom de la sous-sous-famille", "Non", "3", "2", "66.7 %", "2", "1", "50.0 %"),
        // asList(attributeAndValue2.getAttributeName(), attributeAndValue2.getAttributeCode(), attributeAndValue2.getAttributeValueName(),
        // attributeAndValue2.getAttributeValueCode(), "PU - Nom de l'univers", "PF - Nom de la famille", "PSF - Nom de la sous-famille",
        // "PSSF - Nom de la sous-sous-famille", "Oui", "3", "0", "0.0 %", "2", "0", "0.0 %"),
        // emptyList()));
    }

    @Test
    public void createCompletionRateFileWhenTwoPSSFOneAttributeOneValueTwoProductsAndAttributeMappedToTheseProductsAndAttributeValuatedForBothProducts_fourLineShouldBeExportedWith100PercentRates()
            throws IOException {
        // Given
        final String attributeCode1 = "ATTR_1";
        final String attributeCode2 = "ATTR_2";
        when(runOptions.getEligibleClassificationAttributes()).thenReturn(new HashSet<>(asList(attributeCode1, attributeCode2)));

        final CategoryStructureDto category1 = new CategoryStructureDto(universeCode1, universeName1, pfCode1, pfName1, psfCode1, psfName1, pssfCode1,
                pssfName1);
        final CategoryStructureDto category2 = new CategoryStructureDto(universeCode2, universeName2, pfCode2, pfName2, psfCode2, psfName2, pssfCode2,
                pssfName2);

        final AttributeAndValueDto attributeAndValue1 = new AttributeAndValueDto(attributeCode1, "Libellé d'attribut 1", "ATTR_VALUE_1",
                "Libellé valeur d'attribut 1");
        final AttributeAndValueDto attributeAndValue2 = new AttributeAndValueDto(attributeCode2, "Libellé d'attribut 2", "ATTR_VALUE_2",
                "Libellé valeur d'attribut 2");

        final Map<String, Set<AttributeAndValueDto>> pssfsWithValuesForAttr1 = new HashedMap<>();
        pssfsWithValuesForAttr1.put(pssfCode1, new HashSet<>());
        pssfsWithValuesForAttr1.get(pssfCode1).add(attributeAndValue1);

        final Map<String, Set<AttributeAndValueDto>> pssfsWithValuesForAttr2 = new HashedMap<>();
        pssfsWithValuesForAttr2.put(pssfCode2, new HashSet<>());
        pssfsWithValuesForAttr2.get(pssfCode2).add(attributeAndValue2);

        when(rmsArticlesOnlineExtractor.getAllOnlineGlRmsArticlesUg(any())).thenReturn(emptySet());
        when(pssfsFromAttributeExtractor.extractPssfsAndAttributeValues(anyString())).thenReturn(pssfsWithValuesForAttr1, pssfsWithValuesForAttr2);
        when(productAndMoCoNumbersForPssfExtractor.extractProductsNumber(anyString(), any())).thenReturn(1, 1);
        when(productAndMoCoNumbersForPssfExtractor.extractMoCosNumber(anyString(), any())).thenReturn(1, 1);
        when(classificationStructureExtractor.extractPcmStructureForPssf(anyString())).thenReturn(category1, category2);
        // Enjeu du test : les deux produits valorisés mais chacun avec une valeur différente
        when(valuatedAttributesInPssfExtractor.extractValuesByAttributeForPssf(anyString(), anyString(), any())).thenReturn(asList(attributeAndValue1),
                asList(attributeAndValue2));
        when(moCoValuationDetailsExtractor.extractArticleDetailsWithAttributeValuesForPssfAndAttribue(anyString(), anyString(), any())).thenReturn(asList(
                new ArticleDetailsDto(attributeAndValue1, "111", "101010", "100100"), new ArticleDetailsDto(attributeAndValue2, "222", "202020", "200200")));

        // When
        completionRateComputer.createCompletionRateFile();

        // Then
        assertRatesStructure(asList(
                asList(attributeAndValue1.getAttributeName(), attributeAndValue1.getAttributeCode(), attributeAndValue1.getAttributeValueName(),
                        attributeAndValue1.getAttributeValueCode(), "PU_1 - Nom de l'univers 1", "PF_1 - Nom de la famille 1",
                        "PSF_1 - Nom de la sous-famille 1", "PSSF_1 - Nom de la sous-sous-famille 1", "Non", "1", "1", "100.0 %", "1", "1", "100.0 %"),
                asList(attributeAndValue2.getAttributeName(), attributeAndValue2.getAttributeCode(), attributeAndValue2.getAttributeValueName(),
                        attributeAndValue2.getAttributeValueCode(), "PU_2 - Nom de l'univers 2", "PF_2 - Nom de la famille 2",
                        "PSF_2 - Nom de la sous-famille 2", "PSSF_2 - Nom de la sous-sous-famille 2", "Non", "1", "1", "100.0 %", "1", "1", "100.0 %"),
                emptyList()));
        // TODO activer la séparation par attribut GL Online
        // asList(attributeAndValue1.getAttributeName(), attributeAndValue1.getAttributeCode(), attributeAndValue1.getAttributeValueName(),
        // attributeAndValue1.getAttributeValueCode(), "PU_1 - Nom de l'univers 1", "PF_1 - Nom de la famille 1",
        // "PSF_1 - Nom de la sous-famille 1", "PSSF_1 - Nom de la sous-sous-famille 1", "Non", "1", "1", "100.0 %", "1", "1", "100.0 %"),
        // asList(attributeAndValue1.getAttributeName(), attributeAndValue1.getAttributeCode(), attributeAndValue1.getAttributeValueName(),
        // attributeAndValue1.getAttributeValueCode(), "PU_1 - Nom de l'univers 1", "PF_1 - Nom de la famille 1",
        // "PSF_1 - Nom de la sous-famille 1", "PSSF_1 - Nom de la sous-sous-famille 1", "Oui", "1", "0", "0.0 %", "1", "0", "0.0 %"),
        // asList(attributeAndValue2.getAttributeName(), attributeAndValue2.getAttributeCode(), attributeAndValue2.getAttributeValueName(),
        // attributeAndValue2.getAttributeValueCode(), "PU_2 - Nom de l'univers 2", "PF_2 - Nom de la famille 2",
        // "PSF_2 - Nom de la sous-famille 2", "PSSF_2 - Nom de la sous-sous-famille 2", "Non", "1", "1", "100.0 %", "1", "1", "100.0 %"),
        // asList(attributeAndValue2.getAttributeName(), attributeAndValue2.getAttributeCode(), attributeAndValue2.getAttributeValueName(),
        // attributeAndValue2.getAttributeValueCode(), "PU_2 - Nom de l'univers 2", "PF_2 - Nom de la famille 2",
        // "PSF_2 - Nom de la sous-famille 2", "PSSF_2 - Nom de la sous-sous-famille 2", "Oui", "1", "0", "0.0 %", "1", "0", "0.0 %"),
        // emptyList()));
    }

    @Test
    public void createCompletionRateFileWhenTwoPSSFOneAttributeOneValueOnEachPssfAndOneProductsWithAttributeCompleted_fourLineShouldBeExportedWith100PercentRatesForOneAnd0PercentRateForTheOther()
            throws IOException {
        // Given
        final String attributeCode1 = "ATTR_1";
        final String attributeCode2 = "ATTR_2";
        when(runOptions.getEligibleClassificationAttributes()).thenReturn(new HashSet<>(asList(attributeCode1, attributeCode2)));

        final CategoryStructureDto category1 = new CategoryStructureDto(universeCode1, universeName1, pfCode1, pfName1, psfCode1, psfName1, pssfCode1,
                pssfName1);
        final CategoryStructureDto category2 = new CategoryStructureDto(universeCode2, universeName2, pfCode2, pfName2, psfCode2, psfName2, pssfCode2,
                pssfName2);

        final AttributeAndValueDto attributeAndValue1 = new AttributeAndValueDto(attributeCode1, "Libellé d'attribut 1", "ATTR_VALUE_1",
                "Libellé valeur d'attribut 1");
        final AttributeAndValueDto attributeAndValue2 = new AttributeAndValueDto(attributeCode2, "Libellé d'attribut 2", "ATTR_VALUE_2",
                "Libellé valeur d'attribut 2");

        final Map<String, Set<AttributeAndValueDto>> pssfsWithValuesForAttribute1 = new HashedMap<>();
        pssfsWithValuesForAttribute1.put(pssfCode1, new HashSet<>());
        pssfsWithValuesForAttribute1.get(pssfCode1).add(attributeAndValue1);

        final Map<String, Set<AttributeAndValueDto>> pssfsWithValuesForAttribute2 = new HashedMap<>();
        pssfsWithValuesForAttribute2.put(pssfCode2, new HashSet<>());
        pssfsWithValuesForAttribute2.get(pssfCode2).add(attributeAndValue2);

        when(rmsArticlesOnlineExtractor.getAllOnlineGlRmsArticlesUg(any())).thenReturn(emptySet());
        when(pssfsFromAttributeExtractor.extractPssfsAndAttributeValues(anyString())).thenReturn(pssfsWithValuesForAttribute1, pssfsWithValuesForAttribute2);
        when(productAndMoCoNumbersForPssfExtractor.extractProductsNumber(anyString(), any())).thenReturn(1, 1);
        when(productAndMoCoNumbersForPssfExtractor.extractMoCosNumber(anyString(), any())).thenReturn(1, 1);
        when(classificationStructureExtractor.extractPcmStructureForPssf(anyString())).thenReturn(category1, category2);
        // Enjeu du test : seul l'article de la première PSSF a son attribut valorisé
        when(valuatedAttributesInPssfExtractor.extractValuesByAttributeForPssf(anyString(), anyString(), any())).thenReturn(asList(attributeAndValue1),
                emptyList());
        when(moCoValuationDetailsExtractor.extractArticleDetailsWithAttributeValuesForPssfAndAttribue(anyString(), anyString(), any())).thenReturn(asList(
                new ArticleDetailsDto(attributeAndValue1, "111", "101010", "100100"), new ArticleDetailsDto(attributeAndValue2, "222", "202020", "200200")));

        // When
        completionRateComputer.createCompletionRateFile();

        // Then
        assertRatesStructure(asList(
                asList(attributeAndValue1.getAttributeName(), attributeAndValue1.getAttributeCode(), attributeAndValue1.getAttributeValueName(),
                        attributeAndValue1.getAttributeValueCode(), "PU_1 - Nom de l'univers 1", "PF_1 - Nom de la famille 1",
                        "PSF_1 - Nom de la sous-famille 1", "PSSF_1 - Nom de la sous-sous-famille 1", "Non", "1", "1", "100.0 %", "1", "1", "100.0 %"),
                asList(attributeAndValue2.getAttributeName(), attributeAndValue2.getAttributeCode(), attributeAndValue2.getAttributeValueName(),
                        attributeAndValue2.getAttributeValueCode(), "PU_2 - Nom de l'univers 2", "PF_2 - Nom de la famille 2",
                        "PSF_2 - Nom de la sous-famille 2", "PSSF_2 - Nom de la sous-sous-famille 2", "Non", "1", "0", "0.0 %", "1", "0", "0.0 %"),
                emptyList()));
        // TODO activer la séparation par attribut GL Online
        // asList(attributeAndValue1.getAttributeName(), attributeAndValue1.getAttributeCode(), attributeAndValue1.getAttributeValueName(),
        // attributeAndValue1.getAttributeValueCode(), "PU_1 - Nom de l'univers 1", "PF_1 - Nom de la famille 1",
        // "PSF_1 - Nom de la sous-famille 1", "PSSF_1 - Nom de la sous-sous-famille 1", "Non", "1", "1", "100.0 %", "1", "1", "100.0 %"),
        // asList(attributeAndValue1.getAttributeName(), attributeAndValue1.getAttributeCode(), attributeAndValue1.getAttributeValueName(),
        // attributeAndValue1.getAttributeValueCode(), "PU_1 - Nom de l'univers 1", "PF_1 - Nom de la famille 1",
        // "PSF_1 - Nom de la sous-famille 1", "PSSF_1 - Nom de la sous-sous-famille 1", "Oui", "1", "0", "0.0 %", "1", "0", "0.0 %"),
        // asList(attributeAndValue2.getAttributeName(), attributeAndValue2.getAttributeCode(), attributeAndValue2.getAttributeValueName(),
        // attributeAndValue2.getAttributeValueCode(), "PU_2 - Nom de l'univers 2", "PF_2 - Nom de la famille 2",
        // "PSF_2 - Nom de la sous-famille 2", "PSSF_2 - Nom de la sous-sous-famille 2", "Non", "1", "0", "0.0 %", "1", "0", "0.0 %"),
        // asList(attributeAndValue2.getAttributeName(), attributeAndValue2.getAttributeCode(), attributeAndValue2.getAttributeValueName(),
        // attributeAndValue2.getAttributeValueCode(), "PU_2 - Nom de l'univers 2", "PF_2 - Nom de la famille 2",
        // "PSF_2 - Nom de la sous-famille 2", "PSSF_2 - Nom de la sous-sous-famille 2", "Oui", "1", "0", "0.0 %", "1", "0", "0.0 %"),
        // emptyList()));
    }

    @Test
    public void createCompletionRateFileWhenOnePSSFOneAttributeWithTwoValuesAndFiveProductsWithAttributeCompletedForAllAndThreeOfThemWithOneValue_fourLineShouldBeExportedWith60And40PercentRates()
            throws IOException {
        // Given
        final String attributeCode = "ATTR";
        when(runOptions.getEligibleClassificationAttributes()).thenReturn(new HashSet<>(asList(attributeCode)));

        final CategoryStructureDto category = new CategoryStructureDto(universeCode, universeName, pfCode, pfName, psfCode, psfName, pssfCode, pssfName);

        final AttributeAndValueDto attributeAndValue1 = new AttributeAndValueDto(attributeCode, "Libellé d'attribut", "ATTR_VALUE_1",
                "Libellé valeur 1 d'attribut");
        final AttributeAndValueDto attributeAndValue2 = new AttributeAndValueDto(attributeCode, "Libellé d'attribut", "ATTR_VALUE_2",
                "Libellé valeur 2 d'attribut");

        final Map<String, Set<AttributeAndValueDto>> pssfsWithValuesForAttribute = new HashedMap<>();
        pssfsWithValuesForAttribute.put(pssfCode, new HashSet<>());
        pssfsWithValuesForAttribute.get(pssfCode).add(attributeAndValue1);
        pssfsWithValuesForAttribute.get(pssfCode).add(attributeAndValue2);

        when(rmsArticlesOnlineExtractor.getAllOnlineGlRmsArticlesUg(any())).thenReturn(emptySet());
        when(pssfsFromAttributeExtractor.extractPssfsAndAttributeValues(anyString())).thenReturn(pssfsWithValuesForAttribute);
        when(productAndMoCoNumbersForPssfExtractor.extractProductsNumber(anyString(), any())).thenReturn(5, 5);
        when(productAndMoCoNumbersForPssfExtractor.extractMoCosNumber(anyString(), any())).thenReturn(5, 5);
        when(classificationStructureExtractor.extractPcmStructureForPssf(anyString())).thenReturn(category);
        // Enjeu du test : seul l'article de la première PSSF a son attribut valorisé
        when(valuatedAttributesInPssfExtractor.extractValuesByAttributeForPssf(anyString(), anyString(), any()))
                .thenReturn(asList(attributeAndValue1, attributeAndValue1, attributeAndValue1, attributeAndValue2, attributeAndValue2), emptyList());
        when(moCoValuationDetailsExtractor.extractArticleDetailsWithAttributeValuesForPssfAndAttribue(anyString(), anyString(), any())).thenReturn(asList(
                new ArticleDetailsDto(attributeAndValue1, "111", "101010", "100100"), new ArticleDetailsDto(attributeAndValue1, "222", "202020", "200200"),
                new ArticleDetailsDto(attributeAndValue1, "333", "303030", "300300"), new ArticleDetailsDto(attributeAndValue2, "444", "404040", "400400"),
                new ArticleDetailsDto(attributeAndValue2, "555", "505050", "500500")), emptyList());

        // When
        completionRateComputer.createCompletionRateFile();

        // Then
        assertRatesStructure(asList(
                asList(attributeAndValue1.getAttributeName(), attributeAndValue1.getAttributeCode(), attributeAndValue1.getAttributeValueName(),
                        attributeAndValue1.getAttributeValueCode(), "PU - Nom de l'univers", "PF - Nom de la famille", "PSF - Nom de la sous-famille",
                        "PSSF - Nom de la sous-sous-famille", "Non", "5", "3", "60.0 %", "5", "3", "60.0 %"),
                asList(attributeAndValue2.getAttributeName(), attributeAndValue2.getAttributeCode(), attributeAndValue2.getAttributeValueName(),
                        attributeAndValue2.getAttributeValueCode(), "PU - Nom de l'univers", "PF - Nom de la famille", "PSF - Nom de la sous-famille",
                        "PSSF - Nom de la sous-sous-famille", "Non", "5", "2", "40.0 %", "5", "2", "40.0 %"),
                emptyList()));
        // TODO activer la séparation par attribut GL Online
        // assertRatesStructure(asList(
        // asList(attributeAndValue1.getAttributeName(), attributeAndValue1.getAttributeCode(), attributeAndValue1.getAttributeValueName(),
        // attributeAndValue1.getAttributeValueCode(), "PU - Nom de l'univers", "PF - Nom de la famille", "PSF - Nom de la sous-famille",
        // "PSSF - Nom de la sous-sous-famille", "Non", "5", "3", "60.0 %", "5", "3", "60.0 %"),
        // asList(attributeAndValue1.getAttributeName(), attributeAndValue1.getAttributeCode(), attributeAndValue1.getAttributeValueName(),
        // attributeAndValue1.getAttributeValueCode(), "PU - Nom de l'univers", "PF - Nom de la famille", "PSF - Nom de la sous-famille",
        // "PSSF - Nom de la sous-sous-famille", "Oui", "5", "0", "0.0 %", "5", "0", "0.0 %"),
        // asList(attributeAndValue2.getAttributeName(), attributeAndValue2.getAttributeCode(), attributeAndValue2.getAttributeValueName(),
        // attributeAndValue2.getAttributeValueCode(), "PU - Nom de l'univers", "PF - Nom de la famille", "PSF - Nom de la sous-famille",
        // "PSSF - Nom de la sous-sous-famille", "Non", "5", "2", "40.0 %", "5", "2", "40.0 %"),
        // asList(attributeAndValue2.getAttributeName(), attributeAndValue2.getAttributeCode(), attributeAndValue2.getAttributeValueName(),
        // attributeAndValue2.getAttributeValueCode(), "PU - Nom de l'univers", "PF - Nom de la famille", "PSF - Nom de la sous-famille",
        // "PSSF - Nom de la sous-sous-famille", "Oui", "5", "0", "0.0 %", "5", "0", "0.0 %"),
        // emptyList()));
    }

    private void assertRatesStructure(List<List<String>> rows) throws IOException {
        verify(resultExporter).init();
        verify(resultExporter).writeHeader(any());
        verify(resultExporter, times(rows.size())).writeRow(rowDataCapture.capture());
        verify(resultExporter).close();

        List<List<String>> outputRows = rowDataCapture.getAllValues();
        assertThat(outputRows).containsAll(rows);
    }
}
