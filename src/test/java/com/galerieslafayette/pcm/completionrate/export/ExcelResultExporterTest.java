package com.galerieslafayette.pcm.completionrate.export;

import static java.util.Arrays.asList;
import static org.apache.poi.ss.SpreadsheetVersion.EXCEL97;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Files.delete;
import static org.assertj.core.util.Files.newFolder;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.assertj.core.data.Offset;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.galerieslafayette.pcm.completionrate.RunOptions;

@RunWith(MockitoJUnitRunner.class)
public class ExcelResultExporterTest {

    private static final Path BASE_DIRECTORY = Paths.get("target", "output-test");
    private static final String OUPUT_BASE_FILE_NAME = "result";

    private final Instant pseudoCurrentInstant = Instant.now();

    @Mock
    private RunOptions runOptions;

    /** Component under test. */
    private ExcelResultExporter excelResultExporter;

    @Before
    public void setUp() {
        if (BASE_DIRECTORY.toFile().exists()) {
            delete(BASE_DIRECTORY.toFile());
        }
        newFolder(BASE_DIRECTORY.toFile().getAbsolutePath());

        when(runOptions.getOutputDirectory()).thenReturn(BASE_DIRECTORY.toFile().getAbsolutePath());
        when(runOptions.getOutputBaseFileName()).thenReturn(OUPUT_BASE_FILE_NAME);

        // Cas particulier pour tester le comportement d'un 'timestamp'
        excelResultExporter = new ExcelResultExporter(runOptions) {

            @Override
            public Instant getCurrentInstant() {
                return pseudoCurrentInstant;
            }
        };
    }

    @Test
    public void initMustCreateNewFile() throws IOException {
        // When
        excelResultExporter.init();
        excelResultExporter.close();

        // Then
        final File expectedFilePath = BASE_DIRECTORY.resolve(
                OUPUT_BASE_FILE_NAME + LocalDateTime.ofInstant(pseudoCurrentInstant, ZoneId.systemDefault()).format(ResultExporter.TIME_STAMP_FORMAT) + ".xls")
                .toFile();

        assertThat(expectedFilePath).exists();

        try (InputStream input = new FileInputStream(expectedFilePath); Workbook workbook = new HSSFWorkbook(input)) {
            assertThat(workbook.getSpreadsheetVersion()).isEqualTo(EXCEL97);
            assertThat(workbook.getNumberOfSheets()).isEqualTo(1);
            assertThat(workbook.getSheetAt(0).getSheetName()).isEqualTo("Taux de complétion");
        }
    }

    @Test
    public void writeHeaderShouldCreateStringCellsWithThickBorders() throws IOException {
        // Given
        final String header1 = "Entête 1";
        final String header2 = "Entête 2 avec plein de caractères vicieux : ب船éñàçïù€`";
        // On vérifie au passage qu'une référence 'null' devient une chaîne vide
        final String header3 = null;
        final String header4 = "Entête 4";
        excelResultExporter.init();

        // When
        excelResultExporter.writeHeader(asList(header1, header2, header3, header4));
        excelResultExporter.close();

        // Then
        final File expectedFilePath = BASE_DIRECTORY.resolve(
                OUPUT_BASE_FILE_NAME + LocalDateTime.ofInstant(pseudoCurrentInstant, ZoneId.systemDefault()).format(ResultExporter.TIME_STAMP_FORMAT) + ".xls")
                .toFile();

        assertThat(expectedFilePath).exists();

        try (InputStream input = new FileInputStream(expectedFilePath); Workbook workbook = new HSSFWorkbook(input)) {
            assertThat(workbook.getSpreadsheetVersion()).isEqualTo(EXCEL97);
            assertThat(workbook.getNumberOfSheets()).isEqualTo(1);

            Sheet sheet = workbook.getSheetAt(0);
            assertThat(sheet.getRow(0)).isNotNull();
            Row row = sheet.getRow(0);

            assertCellStringValue(header1, row, 0);
            assertCellStringValue(header2, row, 1);
            assertCellStringValue("", row, 2);
            assertCellStringValue(header4, row, 3);

            assertCellBorders(row, 0, BorderStyle.THICK, BorderStyle.THICK, BorderStyle.THIN, BorderStyle.THICK);
            assertCellBorders(row, 1, BorderStyle.THICK, BorderStyle.THIN, BorderStyle.THIN, BorderStyle.THICK);
            assertCellBorders(row, 2, BorderStyle.THICK, BorderStyle.THIN, BorderStyle.THIN, BorderStyle.THICK);
            assertCellBorders(row, 3, BorderStyle.THICK, BorderStyle.THIN, BorderStyle.THICK, BorderStyle.THICK);
        }
    }

    @Test
    public void writeRowForFirstTimeShouldCreateStringCellsWithOnlyLateralBorders() throws IOException {
        // Given
        final String data1 = "Valeur 1";
        final String data2 = "Valeur 2 avec plein de caractères vicieux : ب船éñàçïù€`";
        // On vérifie au passage qu'une référence 'null' devient une chaîne vide
        final String data3 = null;
        final String data4 = "Valeur 4";
        excelResultExporter.init();

        // When
        excelResultExporter.writeRow(asList(data1, data2, data3, data4));
        excelResultExporter.close();

        // Then
        final File expectedFilePath = BASE_DIRECTORY.resolve(
                OUPUT_BASE_FILE_NAME + LocalDateTime.ofInstant(pseudoCurrentInstant, ZoneId.systemDefault()).format(ResultExporter.TIME_STAMP_FORMAT) + ".xls")
                .toFile();

        assertThat(expectedFilePath).exists();

        try (InputStream input = new FileInputStream(expectedFilePath); Workbook workbook = new HSSFWorkbook(input)) {
            assertThat(workbook.getSpreadsheetVersion()).isEqualTo(EXCEL97);
            assertThat(workbook.getNumberOfSheets()).isEqualTo(1);

            Sheet sheet = workbook.getSheetAt(0);
            assertThat(sheet.getRow(1)).isNotNull();
            Row row = sheet.getRow(1);

            assertCellStringValue(data1, row, 0);
            assertCellStringValue(data2, row, 1);
            assertCellStringValue("", row, 2);
            assertCellStringValue(data4, row, 3);

            assertCellBorders(row, 0, BorderStyle.NONE, BorderStyle.THICK, BorderStyle.THIN, BorderStyle.THIN);
            assertCellBorders(row, 1, BorderStyle.NONE, BorderStyle.THIN, BorderStyle.THIN, BorderStyle.THIN);
            assertCellBorders(row, 2, BorderStyle.NONE, BorderStyle.THIN, BorderStyle.THIN, BorderStyle.THIN);
            assertCellBorders(row, 3, BorderStyle.NONE, BorderStyle.THIN, BorderStyle.THICK, BorderStyle.THIN);

            // TODO aligner et formatter les nombres et pourcentages
            // TODO PARTAGER les styles
        }
    }

    @Test
    public void writeRowForNotFirstTimeShouldCreateStringCellsWithBottomMediumBorder() throws IOException {
        // Given
        final String data1 = "Valeur 1";
        final String data2 = "Valeur 2 avec plein de caractères vicieux : ب船éñàçïù€`";
        final String data3 = "Valeur 3";
        final double numberData = 123.45;
        excelResultExporter.init();

        // When
        excelResultExporter.writeRow(asList(data1 + " première ligne", data2 + " première ligne", data3 + " première ligne", numberData + " première ligne"));
        excelResultExporter.writeRow(asList(data1, data2, data3, String.valueOf(numberData)));
        excelResultExporter.close();

        // Then
        final File expectedFilePath = BASE_DIRECTORY.resolve(
                OUPUT_BASE_FILE_NAME + LocalDateTime.ofInstant(pseudoCurrentInstant, ZoneId.systemDefault()).format(ResultExporter.TIME_STAMP_FORMAT) + ".xls")
                .toFile();

        assertThat(expectedFilePath).exists();

        try (InputStream input = new FileInputStream(expectedFilePath); Workbook workbook = new HSSFWorkbook(input)) {
            assertThat(workbook.getSpreadsheetVersion()).isEqualTo(EXCEL97);
            assertThat(workbook.getNumberOfSheets()).isEqualTo(1);

            Sheet sheet = workbook.getSheetAt(0);
            assertThat(sheet.getRow(1)).isNotNull();
            assertThat(sheet.getRow(2)).isNotNull();
            Row row = sheet.getRow(2);

            assertCellStringValue(data1, row, 0);
            assertCellStringValue(data2, row, 1);
            assertCellStringValue(data3, row, 2);
            assertCellNumberValue(numberData, row, 3);

            assertCellBorders(row, 0, BorderStyle.NONE, BorderStyle.THICK, BorderStyle.THIN, BorderStyle.THIN);
            assertCellBorders(row, 1, BorderStyle.NONE, BorderStyle.THIN, BorderStyle.THIN, BorderStyle.THIN);
            assertCellBorders(row, 2, BorderStyle.NONE, BorderStyle.THIN, BorderStyle.THIN, BorderStyle.THIN);
            assertCellBorders(row, 3, BorderStyle.NONE, BorderStyle.THIN, BorderStyle.THICK, BorderStyle.THIN);

            // TODO aligner et formatter les nombres et pourcentages
            // TODO PARTAGER les styles
        }
    }

    private void assertCellStringValue(final String value, Row row, int columnIndex) {
        assertThat(row.getCell(columnIndex)).isNotNull();
        assertThat(row.getCell(columnIndex).getStringCellValue()).isEqualTo(value);
    }

    private void assertCellNumberValue(final double value, Row row, int columnIndex) {
        assertThat(row.getCell(columnIndex)).isNotNull();
        assertThat(row.getCell(columnIndex).getNumericCellValue()).isEqualTo(value, Offset.<Double>offset(.1));
    }

    private void assertCellBorders(Row row, int coumnIndex, BorderStyle borderTopStyle, BorderStyle borderLeftStyle, BorderStyle borderRigthStyle,
            BorderStyle borderBottomStyle) {
        CellStyle style = row.getCell(coumnIndex).getCellStyle();

        assertThat(style.getAlignmentEnum()).isEqualTo(HorizontalAlignment.CENTER);
        assertThat(style.getBorderTopEnum()).isEqualTo(borderTopStyle);
        assertThat(style.getBorderLeftEnum()).isEqualTo(borderLeftStyle);
        assertThat(style.getBorderRightEnum()).isEqualTo(borderRigthStyle);
        assertThat(style.getBorderBottomEnum()).isEqualTo(borderBottomStyle);
    }
}