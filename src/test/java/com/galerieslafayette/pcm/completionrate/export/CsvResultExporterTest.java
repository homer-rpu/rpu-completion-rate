package com.galerieslafayette.pcm.completionrate.export;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.unmodifiableList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Files.delete;
import static org.assertj.core.util.Files.newFolder;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.galerieslafayette.pcm.completionrate.RunOptions;

@RunWith(MockitoJUnitRunner.class)
public class CsvResultExporterTest {

    private static final Path BASE_DIRECTORY = Paths.get("target", "output-test");

    private final Instant pseudoCurrentInstant = Instant.now();

    @Mock
    private RunOptions runOptions;

    /** Component under test. */
    private CsvResultExporter csvResultExporter;

    @Before
    public void setUp() {
        if (BASE_DIRECTORY.toFile().exists()) {
            delete(BASE_DIRECTORY.toFile());
        }
        newFolder(BASE_DIRECTORY.toFile().getAbsolutePath());

        // Cas particulier pour tester le comportement d'un 'timestamp'
        csvResultExporter = new CsvResultExporter(runOptions) {

            @Override
            public Instant getCurrentInstant() {
                return pseudoCurrentInstant;
            }
        };
    }

    @Test
    public void initMustCreateNewFile() throws IOException {
        // Given
        final String outputBaseFileName = "result";
        when(runOptions.getOutputDirectory()).thenReturn(BASE_DIRECTORY.toFile().getAbsolutePath());
        when(runOptions.getOutputBaseFileName()).thenReturn(outputBaseFileName);
        when(runOptions.getOutputEncoding()).thenReturn(StandardCharsets.UTF_8.name());

        // When
        csvResultExporter.init();
        csvResultExporter.close();

        // Then
        assertThat(
                BASE_DIRECTORY
                        .resolve(outputBaseFileName
                                + LocalDateTime.ofInstant(pseudoCurrentInstant, ZoneId.systemDefault()).format(ResultExporter.TIME_STAMP_FORMAT) + ".csv")
                        .toFile()).exists();
    }

    @Test
    public void writeHeaderShouldRespectEncodingInOutputFile() throws IOException {
        // Given
        final String outputBaseFileName = "result";
        final Charset charset = StandardCharsets.UTF_8;
        // On vérifie au passage qu'une référence 'null' devient une chaîne vide
        final List<String> headers = unmodifiableList(asList("Header 1", null, "Header 3 avec plein de caractères vicieux : ب船é\"ñàçïù€`"));

        when(runOptions.getOutputDirectory()).thenReturn(BASE_DIRECTORY.toFile().getAbsolutePath());
        when(runOptions.getOutputBaseFileName()).thenReturn(outputBaseFileName);
        when(runOptions.getOutputEncoding()).thenReturn(charset.name());

        csvResultExporter.init();

        // When
        csvResultExporter.writeHeader(headers);
        csvResultExporter.close();

        // Then
        assertThat(
                BASE_DIRECTORY
                        .resolve(outputBaseFileName
                                + LocalDateTime.ofInstant(pseudoCurrentInstant, ZoneId.systemDefault()).format(ResultExporter.TIME_STAMP_FORMAT) + ".csv")
                        .toFile()).usingCharset(charset).hasContent("\"Header 1\";\"\";\"Header 3 avec plein de caractères vicieux : ب船é\"\"ñàçïù€`\"");
    }

    @Test
    public void lastRowShouldBeEmpty() throws IOException {
        // Given
        final String outputBaseFileName = "result";
        final Charset charset = StandardCharsets.UTF_8;

        when(runOptions.getOutputDirectory()).thenReturn(BASE_DIRECTORY.toFile().getAbsolutePath());
        when(runOptions.getOutputBaseFileName()).thenReturn(outputBaseFileName);
        when(runOptions.getOutputEncoding()).thenReturn(charset.name());

        csvResultExporter.init();

        // When
        csvResultExporter.writeRow(emptyList());
        csvResultExporter.close();

        // Then
        assertThat(
                BASE_DIRECTORY
                        .resolve(outputBaseFileName
                                + LocalDateTime.ofInstant(pseudoCurrentInstant, ZoneId.systemDefault()).format(ResultExporter.TIME_STAMP_FORMAT) + ".csv")
                        .toFile()).usingCharset(charset).hasContent("");
    }

    @Test
    public void writeRowShouldRespectEncodingInOutputFile() throws IOException {
        // Given
        final String outputBaseFileName = "result";
        final Charset charset = StandardCharsets.UTF_8;
        // On vérifie au passage qu'une référence 'null' devient une chaîne vide
        final List<String> headers = unmodifiableList(asList("Information 1", null, "Information 3 avec plein de caractères vicieux : ب船é\"ñàçïù€`"));

        when(runOptions.getOutputDirectory()).thenReturn(BASE_DIRECTORY.toFile().getAbsolutePath());
        when(runOptions.getOutputBaseFileName()).thenReturn(outputBaseFileName);
        when(runOptions.getOutputEncoding()).thenReturn(charset.name());

        csvResultExporter.init();

        // When
        csvResultExporter.writeRow(headers);
        csvResultExporter.close();

        // Then
        assertThat(
                BASE_DIRECTORY
                        .resolve(outputBaseFileName
                                + LocalDateTime.ofInstant(pseudoCurrentInstant, ZoneId.systemDefault()).format(ResultExporter.TIME_STAMP_FORMAT) + ".csv")
                        .toFile()).usingCharset(charset)
                                .hasContent("\n\"Information 1\";\"\";\"Information 3 avec plein de caractères vicieux : ب船é\"\"ñàçïù€`\"");
        /*
         * On utilise un retour charriot en début de ligne plutôt qu'en fin car la méthode readline des API standard ne "voit pas" le dernier retour charriot
         * d'un fichier
         */
    }
}
