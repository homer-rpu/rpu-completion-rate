package com.galerieslafayette.pcm.completionrate.data.ecom;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;

import com.galerieslafayette.pcm.completionrate.data.AttributeAndValueDto;

@RunWith(MockitoJUnitRunner.class)
public class ValuatedAttributesInPssfExtractorTest {

    @Mock
    private NamedParameterJdbcOperations ecomJdbcTemplate;

    private ValuatedAttributesInPssfExtractor valuatedAttributesInPssfExtractor;

    @Before
    public void setUp() {
        valuatedAttributesInPssfExtractor = new ValuatedAttributesInPssfExtractor(ecomJdbcTemplate);
    }

    @Test
    public void resultShouldBeEmptyIfPssfIsUnknow() {
        // Given
        when(ecomJdbcTemplate.query(anyString(), any(MapSqlParameterSource.class), Mockito.<RowMapper<Integer>>any())).thenReturn(emptyList());

        // When
        List<AttributeAndValueDto> result = valuatedAttributesInPssfExtractor.extractValuesByAttributeForPssf("UNKNOWN_PSSF", "UNKNOWN_ATTRIBUTE",
                new HashSet<>(asList("SEASONS_FILTER")));

        // Then
        assertThat(result).isEmpty();
    }

    @Test
    public void resultShouldContainOneValueIfOneProductValuatedForPssf() {
        // Given
        final String pssf = "PSSF";
        final String attributeCode = "ATTR_CODE";
        final String attributeName = "ATTR_NAME";
        final String attributeValueCode = "ATTR_VAL_CODE";
        final String attributeValueName = "ATTR_VAL_NAME";

        when(ecomJdbcTemplate.query(anyString(), any(MapSqlParameterSource.class), Mockito.<RowMapper<AttributeAndValueDto>>any()))
                .thenReturn(Collections.singletonList(new AttributeAndValueDto(attributeCode, attributeName, attributeValueCode, attributeValueName)));

        // When
        List<AttributeAndValueDto> result = valuatedAttributesInPssfExtractor.extractValuesByAttributeForPssf(pssf, attributeCode,
                new HashSet<>(asList("SEASONS_FILTER")));

        // Then
        assertThat(result).hasSize(1);
        assertThat(result.get(0)).isEqualTo(new AttributeAndValueDto(attributeCode, attributeName, attributeValueCode, attributeValueName));
    }
}
