package com.galerieslafayette.pcm.completionrate.data.ecom;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.galerieslafayette.pcm.completionrate.data.AttributeAndValueDto;
import com.galerieslafayette.pcm.completionrate.data.ecom.ClassificationCategoryAndAttributeValuesForAttributeRowMapper;

@RunWith(MockitoJUnitRunner.class)
public class ClassificationCategoryAndAttributeValuesForAttributeRowMapperTest {

    private static final String ATTRIBUTE_CODE = "ATTR";
    private static final String ATTRIBUTE_NAME = ATTRIBUTE_CODE + " name";

    @Mock
    private ResultSet resultSet;

    private Map<String, Set<AttributeAndValueDto>> map = new HashMap<>();

    private ClassificationCategoryAndAttributeValuesForAttributeRowMapper rowMapperUnderTest = new ClassificationCategoryAndAttributeValuesForAttributeRowMapper(
            ATTRIBUTE_CODE, map);

    @Test(expected = NullPointerException.class)
    public void rowMapperCannotBeconstructedIfMapIsNull() {
        new ClassificationCategoryAndAttributeValuesForAttributeRowMapper(ATTRIBUTE_CODE, null);
    }

    @Test
    public void ifAttributeHasOneClassificationCategoryAndHasOneValueResultShouldHaveOneElement() throws SQLException {
        final String classificationCategoryCode = "CLA";
        final String attributeValueCode = ATTRIBUTE_CODE + "_VAL";
        final String attributeValueName = attributeValueCode + " name";
        when(resultSet.getString(anyString())).thenReturn(classificationCategoryCode, ATTRIBUTE_NAME, attributeValueCode, attributeValueName);

        rowMapperUnderTest.mapRow(resultSet, 1);

        assertThat(map).hasSize(1);
        assertMapContainsEntry(classificationCategoryCode, singletonList(attributeValueCode), singletonList(attributeValueName));
    }

    @Test
    public void ifAttributeHasOneClassificationCategoryAndHasTwoValuesResultShouldHaveOneElementWithOneSetContainingTwoElements() throws SQLException {
        // Given
        final String classificationCategoryCode = "CLA";

        final String attributeValue1Code = ATTRIBUTE_CODE + "_VAL_1";
        final String attributeValue1Name = attributeValue1Code + " name";
        final String attributeValue2Code = ATTRIBUTE_CODE + "_VAL_2";
        final String attributeValue2Name = attributeValue2Code + " name";

        when(resultSet.getString(anyString())).thenReturn(//
                classificationCategoryCode, ATTRIBUTE_NAME, attributeValue1Code, attributeValue1Name, //
                classificationCategoryCode, ATTRIBUTE_NAME, attributeValue2Code, attributeValue2Name);

        // When
        rowMapperUnderTest.mapRow(resultSet, 1);
        rowMapperUnderTest.mapRow(resultSet, 2);

        // Then
        assertThat(map).hasSize(1);
        assertMapContainsEntry(classificationCategoryCode, asList(attributeValue1Code, attributeValue2Code), asList(attributeValue1Name, attributeValue2Name));
    }

    @Test
    public void ifAttributeHasTwoClassificationCategoriesAndEachHasOneValueResultShouldHaveTwoElementsEachWithOneSetContainingOneElement() throws SQLException {
        // Given
        final String classificationCategoryCode1 = "CLA_1";
        final String attributeValue1Code = ATTRIBUTE_CODE + "_VAL_1";
        final String attributeValue1Name = attributeValue1Code + " name";

        final String classificationCategoryCode2 = "CLA_2";
        final String attributeValue2Code = ATTRIBUTE_CODE + "_VAL_2";
        final String attributeValue2Name = attributeValue2Code + " name";

        when(resultSet.getString(anyString())).thenReturn( //
                classificationCategoryCode1, ATTRIBUTE_NAME, attributeValue1Code, attributeValue1Name, //
                classificationCategoryCode2, ATTRIBUTE_NAME, attributeValue2Code, attributeValue2Name);

        // When
        rowMapperUnderTest.mapRow(resultSet, 1);
        rowMapperUnderTest.mapRow(resultSet, 2);

        // Then
        assertThat(map).hasSize(2);
        assertMapContainsEntry(classificationCategoryCode1, singletonList(attributeValue1Code), singletonList(attributeValue1Name));
        assertMapContainsEntry(classificationCategoryCode2, singletonList(attributeValue2Code), singletonList(attributeValue2Name));
    }

    @Test
    public void ifAttributeHasTwoClassificationCategoriesAndEachHasItsOwnAttributeValuesSetResultShouldHaveTwoElementsEachWithOneSetContainingGoodElements()
            throws SQLException {
        // Given
        final String classificationCategoryCode1 = "CLA_1";
        final String attribute1Value1Code = ATTRIBUTE_CODE + "_1_VAL_1";
        final String attribute1Value1Name = attribute1Value1Code + " name";
        final String attribute1Value2Code = ATTRIBUTE_CODE + "_1_VAL_2";
        final String attribute1Value2Name = attribute1Value2Code + " name";
        final String attribute1Value3Code = ATTRIBUTE_CODE + "_1_VAL_3";
        final String attribute1Value3Name = attribute1Value3Code + " name";

        final String classificationCategoryCode2 = "CLA_2";
        final String attribute2Value1Code = ATTRIBUTE_CODE + "_2_VAL_1";
        final String attribute2Value1Name = attribute2Value1Code + " name";
        final String attribute2Value2Code = ATTRIBUTE_CODE + "_2_VAL_2";
        final String attribute2Value2Name = attribute2Value2Code + " name";

        when(resultSet.getString(anyString())).thenReturn( //
                classificationCategoryCode1, ATTRIBUTE_NAME, attribute1Value1Code, attribute1Value1Name, //
                classificationCategoryCode1, ATTRIBUTE_NAME, attribute1Value2Code, attribute1Value2Name, //
                classificationCategoryCode1, ATTRIBUTE_NAME, attribute1Value3Code, attribute1Value3Name, //
                classificationCategoryCode2, ATTRIBUTE_NAME, attribute2Value1Code, attribute2Value1Name, //
                classificationCategoryCode2, ATTRIBUTE_NAME, attribute2Value2Code, attribute2Value2Name);

        // When
        for (int i = 0; i < 5; i++) {
            rowMapperUnderTest.mapRow(resultSet, i);
        }

        // Then
        assertThat(map).hasSize(2);
        assertMapContainsEntry(classificationCategoryCode1, asList(attribute1Value1Code, attribute1Value2Code, attribute1Value3Code),
                asList(attribute1Value1Name, attribute1Value2Name, attribute1Value3Name));
        assertMapContainsEntry(classificationCategoryCode2, asList(attribute2Value1Code, attribute2Value2Code),
                asList(attribute2Value1Name, attribute2Value2Name));
    }

    private void assertMapContainsEntry(final String classificationCategoryCode, final List<String> attributeValueCodes,
            final List<String> attributeValueNames) {
        assertThat(map).containsKey(classificationCategoryCode);
        assertThat(map.get(classificationCategoryCode)).hasSize(attributeValueCodes.size());
        assertThat(map.get(classificationCategoryCode)).hasSize(attributeValueNames.size());
        for (int i = 0; i < attributeValueCodes.size(); i++) {
            assertThat(map.get(classificationCategoryCode))
                    .contains(new AttributeAndValueDto(ATTRIBUTE_CODE, ATTRIBUTE_NAME, attributeValueCodes.get(i), attributeValueNames.get(i)));
        }
    }
}
