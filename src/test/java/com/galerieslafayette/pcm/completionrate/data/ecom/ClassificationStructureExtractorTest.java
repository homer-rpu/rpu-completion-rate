package com.galerieslafayette.pcm.completionrate.data.ecom;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.galerieslafayette.pcm.completionrate.data.CategoryStructureDto;
import com.galerieslafayette.pcm.completionrate.data.ecom.ClassificationStructureExtractor;

@RunWith(MockitoJUnitRunner.class)
public class ClassificationStructureExtractorTest {

    @Mock
    private JdbcTemplate ecomJdbcTemplate;

    private ClassificationStructureExtractor classificationStructureExtractor;

    @Before
    public void setUp() {
        classificationStructureExtractor = new ClassificationStructureExtractor(ecomJdbcTemplate);
    }

    @Test
    public void whenPssfInUnknownResultShouldBeEmpty() {
        // Given
        when(ecomJdbcTemplate.query(anyString(), any(Object[].class), Mockito.<RowMapper<Integer>>any())).thenReturn(emptyList());

        // When an Then
        assertThat(classificationStructureExtractor.extractPcmStructureForPssf("UNKNOWN")).isNull();
    }

    @Test
    public void pcmCategoriesStructureShouldBeRespectedWhenPssfIsFound() {
        // Given
        final String pssf = "PSSF";
        final String pssfName = pssf + "NAME";
        final String psf = "PSF";
        final String psfName = psf + "NAME";
        final String pf = "PF";
        final String pfName = pf + "NAME";
        final String pu = "PU";
        final String puName = pu + "NAME";

        when(ecomJdbcTemplate.query(anyString(), any(Object[].class), Mockito.<RowMapper<CategoryStructureDto>>any()))
                .thenReturn(singletonList(new CategoryStructureDto(pu, puName, pf, pfName, psf, psfName, pssf, pssfName)));

        // When
        CategoryStructureDto result = classificationStructureExtractor.extractPcmStructureForPssf(pssf);

        // Then
        assertThat(result).isNotNull().extracting(CategoryStructureDto::getPssfCode, CategoryStructureDto::getPsfCode, CategoryStructureDto::getPfCode,
                CategoryStructureDto::getUniverseCode).containsExactly(pssf, psf, pf, pu);
        assertThat(result).extracting(CategoryStructureDto::getPssfName, CategoryStructureDto::getPsfName, CategoryStructureDto::getPfName,
                CategoryStructureDto::getUniverseName).containsExactly(pssfName, psfName, pfName, puName);
    }
}
