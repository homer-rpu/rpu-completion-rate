package com.galerieslafayette.pcm.completionrate.data.ecom;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.galerieslafayette.pcm.completionrate.data.AttributeAndValueDto;
import com.galerieslafayette.pcm.completionrate.data.ecom.ClassificationCategoryAndAttributeValuesForAttributeExtractor;
import com.galerieslafayette.pcm.completionrate.data.ecom.PssfsFromAttributeExtractor;

@RunWith(MockitoJUnitRunner.class)
public class PssfsFromAttributeExtractorTest {

    @Mock
    private JdbcTemplate ecomJdbcTemplate;
    @Mock
    private ClassificationCategoryAndAttributeValuesForAttributeExtractor classificationCategoryAndAttributeValuesForAttributeExtractor;

    private PssfsFromAttributeExtractor extractorUnderTest;

    @Before
    public void setUp() {
        extractorUnderTest = new PssfsFromAttributeExtractor(ecomJdbcTemplate, classificationCategoryAndAttributeValuesForAttributeExtractor);
    }

    @Test
    public void whenAttributeDoesNotExistResultShouldBeEmpty() {
        // Given
        when(classificationCategoryAndAttributeValuesForAttributeExtractor.extractClassificationCategoriesWithTheiAttributeValuesForAttribute(anyString()))
                .thenReturn(new HashMap<>());

        // When
        Map<String, Set<AttributeAndValueDto>> result = extractorUnderTest.extractPssfsAndAttributeValues("UNKNOWN");

        // Then
        assertThat(result).isEmpty();
    }

    @Test
    public void whenAttributeIsAssociatedWithClassificationWithoutSubCategoryResultShouldBeEmpty() {
        // Given
        final String cla = "CLA";

        final Map<String, Set<AttributeAndValueDto>> attributesByClassifications = new HashMap<>();
        attributesByClassifications.put(cla, new HashSet<>(singleton(new AttributeAndValueDto("ATTR", "", "ATTR_VAL", ""))));

        when(classificationCategoryAndAttributeValuesForAttributeExtractor.extractClassificationCategoriesWithTheiAttributeValuesForAttribute(anyString()))
                .thenReturn(attributesByClassifications);

        when(ecomJdbcTemplate.query(anyString(), any(Object[].class), Mockito.<RowMapper<String>>any())).thenReturn(emptyList());

        // When
        Map<String, Set<AttributeAndValueDto>> result = extractorUnderTest.extractPssfsAndAttributeValues("ATTRIBUTE_CODE");

        // Then
        assertThat(result).isEmpty();
        verify(ecomJdbcTemplate).query(anyString(), any(Object[].class), Mockito.<RowMapper<String>>any());
    }

    @Test
    public void whenAttributeIsAssociatedWithClassificationWichHasOneSubCategoryPssfResultShouldContainsThePssfCode() {
        // Given
        final String cla = "CLA";

        final Map<String, Set<AttributeAndValueDto>> attributesByClassifications = new HashMap<>();
        attributesByClassifications.put(cla, new HashSet<>(singleton(new AttributeAndValueDto("ATTR", "", "ATTR_VAL", ""))));

        when(classificationCategoryAndAttributeValuesForAttributeExtractor.extractClassificationCategoriesWithTheiAttributeValuesForAttribute(anyString()))
                .thenReturn(attributesByClassifications);

        final String pssf = "PSSF";
        when(ecomJdbcTemplate.query(anyString(), any(Object[].class), Mockito.<RowMapper<String>>any())).thenReturn(singletonList(pssf), emptyList());

        // When
        Map<String, Set<AttributeAndValueDto>> result = extractorUnderTest.extractPssfsAndAttributeValues("ATTRIBUTE_CODE");

        // Then
        assertThat(result).containsEntry(pssf, attributesByClassifications.get(cla));
        verify(ecomJdbcTemplate).query(anyString(), any(Object[].class), Mockito.<RowMapper<String>>any());
    }

    @Test
    public void whenAttributeIsAssociatedWithClassificationWichHasOneSubCategoryNotPssfWichHasOneSubCategoryPssfResultShouldContainsThePssfCode() {
        // Given
        final String cla1 = "CLA1";

        final Map<String, Set<AttributeAndValueDto>> attributesByClassifications = new HashMap<>();
        attributesByClassifications.put(cla1, new HashSet<>(singleton(new AttributeAndValueDto("ATTR", "", "ATTR_VAL", ""))));

        when(classificationCategoryAndAttributeValuesForAttributeExtractor.extractClassificationCategoriesWithTheiAttributeValuesForAttribute(anyString()))
                .thenReturn(attributesByClassifications);

        final String cla2 = "CLA2";
        final String pssf = "PSSF";
        when(ecomJdbcTemplate.query(anyString(), any(Object[].class), Mockito.<RowMapper<String>>any())).thenReturn(singletonList(cla2), singletonList(pssf),
                emptyList());

        // When
        Map<String, Set<AttributeAndValueDto>> result = extractorUnderTest.extractPssfsAndAttributeValues("ATTRIBUTE_CODE");

        // Then
        assertThat(result).containsEntry(pssf, attributesByClassifications.get(cla1));
        verify(ecomJdbcTemplate, times(2)).query(anyString(), any(Object[].class), Mockito.<RowMapper<String>>any());
    }

    @Test
    public void whenAttributeIsAssociatedWithClassificationSubCategoryWithSubCategoryNotPssfResultShouldBeEmpty() {
        // Given
        final String cla1 = "CLA_1";
        final String cla2 = "CLA_2";

        final Map<String, Set<AttributeAndValueDto>> attributesByClassifications = new HashMap<>();
        attributesByClassifications.put(cla1, new HashSet<>(singleton(new AttributeAndValueDto("ATTR_1", "", "ATTR_1_VAL", ""))));
        attributesByClassifications.put(cla2, new HashSet<>(singleton(new AttributeAndValueDto("ATTR_2", "", "ATTR_2_VAL", ""))));

        when(classificationCategoryAndAttributeValuesForAttributeExtractor.extractClassificationCategoriesWithTheiAttributeValuesForAttribute(anyString()))
                .thenReturn(attributesByClassifications);

        when(ecomJdbcTemplate.query(anyString(), any(Object[].class), Mockito.<RowMapper<String>>any())).thenReturn(emptyList(), emptyList());

        // When
        Map<String, Set<AttributeAndValueDto>> result = extractorUnderTest.extractPssfsAndAttributeValues("ATTRIBUTE_CODE");

        // Then
        assertThat(result).isEmpty();
        verify(ecomJdbcTemplate, times(2)).query(anyString(), any(Object[].class), Mockito.<RowMapper<String>>any());
    }

    @Test
    public void whenAttributeIsAssociatedWithClassificationWichHasTwoSubCategoriesPssfsResultShouldContainsBothPssfCodes() {
        // Given
        final String cla = "CLA";
        final String attributeCode = "ATTR";
        final String attributeName = attributeCode + " name";
        final String attributeValueCode = attributeCode + "_VAL";
        final String attributeValueName = attributeValueCode + " name";

        final Map<String, Set<AttributeAndValueDto>> attributesByClassifications = new HashMap<>();
        attributesByClassifications.put(cla,
                new HashSet<>(singleton(new AttributeAndValueDto(attributeCode, attributeName, attributeValueCode, attributeValueName))));

        when(classificationCategoryAndAttributeValuesForAttributeExtractor.extractClassificationCategoriesWithTheiAttributeValuesForAttribute(anyString()))
                .thenReturn(attributesByClassifications);

        final String pssf1 = "PSSF1";
        final String pssf2 = "PSSF2";
        when(ecomJdbcTemplate.query(anyString(), any(Object[].class), Mockito.<RowMapper<String>>any())).thenReturn(asList(pssf1, pssf2));

        // When
        Map<String, Set<AttributeAndValueDto>> result = extractorUnderTest.extractPssfsAndAttributeValues("ATTRIBUTE_CODE");

        // Then
        assertThat(result).containsEntry(pssf1, attributesByClassifications.get(cla)).containsEntry(pssf2, attributesByClassifications.get(cla));
        verify(ecomJdbcTemplate).query(anyString(), any(Object[].class), Mockito.<RowMapper<String>>any());
    }

    @Test
    public void whenAttributeIsAssociatedWithTwoClassificationsWichHasEachTheirOwnAttributeValuesSetAndASubCategoryPssfsResultShouldContainsBothPssfCodesAssociatedWithGooAttributeValuesList() {
        // Given
        final String cla1 = "CLA_1";
        final AttributeAndValueDto attribute1AndValue1 = new AttributeAndValueDto("ATTR_1", "ATTR_1 name", "ATTR_1_VAL_1", "ATTR_1_VAL_1 name");
        final AttributeAndValueDto attribute1AndValue2 = new AttributeAndValueDto("ATTR_1", "ATTR_1 name", "ATTR_1_VAL_2", "ATTR_1_VAL_2 name");
        final AttributeAndValueDto attribute1AndValue3 = new AttributeAndValueDto("ATTR_1", "ATTR_1 name", "ATTR_1_VAL_3", "ATTR_1_VAL_3 name");
        final String cla2 = "CLA_2";
        final AttributeAndValueDto attribute2AndValue1 = new AttributeAndValueDto("ATTR_2", "ATTR_2 name", "ATTR_2_VAL_1", "ATTR_2_VAL_1 name");
        final AttributeAndValueDto attribute2AndValue2 = new AttributeAndValueDto("ATTR_2", "ATTR_2 name", "ATTR_2_VAL_2", "ATTR_2_VAL_2 name");

        // Bien prendre une "LinkedHashMap" pour faire conserver l'ordre des recherches d'éléments entre CLA1 -> PSSF1 et CLA2 -> PSSF2
        // (L'utilisation des "argument matchers" avec Mockito pose problème pour les arguments de type tableau d'objets)
        final Map<String, Set<AttributeAndValueDto>> attributesByClassifications = new LinkedHashMap<>();
        attributesByClassifications.put(cla1, new HashSet<>(asList(attribute1AndValue1, attribute1AndValue2, attribute1AndValue3)));
        attributesByClassifications.put(cla2, new HashSet<>(asList(attribute2AndValue1, attribute2AndValue2)));

        when(classificationCategoryAndAttributeValuesForAttributeExtractor.extractClassificationCategoriesWithTheiAttributeValuesForAttribute(anyString()))
                .thenReturn(attributesByClassifications);

        final String pssf1 = "PSSF1";
        final String pssf2 = "PSSF2";
        when(ecomJdbcTemplate.query(anyString(), any(Object[].class), Mockito.<RowMapper<String>>any())).thenReturn(singletonList(pssf1), singletonList(pssf2));

        // When
        Map<String, Set<AttributeAndValueDto>> result = extractorUnderTest.extractPssfsAndAttributeValues("ATTRIBUTE_CODE");

        // Then
        assertThat(result).containsKey(pssf1);
        assertThat(result.get(pssf1)).contains(attribute1AndValue1, attribute1AndValue2, attribute1AndValue3);

        assertThat(result).containsKey(pssf2);
        assertThat(result.get(pssf2)).contains(attribute2AndValue1, attribute2AndValue2);

        verify(ecomJdbcTemplate, times(2)).query(anyString(), any(Object[].class), Mockito.<RowMapper<String>>any());
    }
}
