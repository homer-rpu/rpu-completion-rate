package com.galerieslafayette.pcm.completionrate.data.ecom;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;

import com.galerieslafayette.pcm.completionrate.data.AttributeAndValueDto;

@RunWith(MockitoJUnitRunner.class)
public class MoCoValuationDetailsExtractorTest {

    @Mock
    private NamedParameterJdbcOperations ecomJdbcTemplate;

    private MoCoValuationDetailsExtractor extractorUnderTest;

    @Before
    public void setUp() {
        extractorUnderTest = new MoCoValuationDetailsExtractor(ecomJdbcTemplate);
    }

    @Test
    public void extractionShouldBeEmptyIfNoArticlesValuatedForPssfAndAttribute() {
        when(ecomJdbcTemplate.query(anyString(), any(MapSqlParameterSource.class), Mockito.<RowMapper<ArticleDetailsDto>>any())).thenReturn(emptyList());

        assertThat(extractorUnderTest.extractArticleDetailsWithAttributeValuesForPssfAndAttribue("PSSF", "ATTR_CODE", new HashSet<>(asList("SEASONS_FILTER"))))
                .isEmpty();
    }

    @Test
    public void extractionShouldContainsDataFound() {
        // Given
        final String pssf = "PSSF";
        final String attributeCode = "ATTR";

        final ArticleDetailsDto article1 = new ArticleDetailsDto(
                new AttributeAndValueDto(attributeCode, "attributeName", "attributeValueCode1", "attributeValueName1"), "1111", "10101010", "100100");
        final ArticleDetailsDto article2 = new ArticleDetailsDto(
                new AttributeAndValueDto(attributeCode, "attributeName", "attributeValueCode2", "attributeValueName2"), "2222", "20202020", "200200");

        when(ecomJdbcTemplate.query(anyString(), any(MapSqlParameterSource.class), Mockito.<RowMapper<ArticleDetailsDto>>any()))
                .thenReturn(Arrays.asList(article1, article2));

        // When
        List<ArticleDetailsDto> result = extractorUnderTest.extractArticleDetailsWithAttributeValuesForPssfAndAttribue(pssf, attributeCode,
                new HashSet<>(asList("SEASONS_FILTER")));

        // Then
        assertThat(result).hasSize(2);
        assertArticleDetails(result, 0, attributeCode, article1);
        assertArticleDetails(result, 1, attributeCode, article2);
    }

    private void assertArticleDetails(List<ArticleDetailsDto> result, final int index, final String attributeCode, final ArticleDetailsDto article) {
        assertThat(result.get(index).getAttributeAndValue().getAttributeCode()).isEqualTo(attributeCode);
        assertThat(result.get(index).getAttributeAndValue().getAttributeValueCode()).isEqualTo(article.getAttributeAndValue().getAttributeValueCode());
        assertThat(result.get(index).getUgArticle()).isEqualTo(article.getUgArticle());
        assertThat(result.get(index).getUgProduct()).isEqualTo(article.getUgProduct());
        assertThat(result.get(index).getColorPk()).isEqualTo(article.getColorPk());
    }
}
