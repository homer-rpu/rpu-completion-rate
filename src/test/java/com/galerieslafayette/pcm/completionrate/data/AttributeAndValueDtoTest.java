package com.galerieslafayette.pcm.completionrate.data;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import com.galerieslafayette.pcm.completionrate.data.AttributeAndValueDto;

/**
 * L'intérêt de cette classe de test est de vérifier qu'on n'a pas perdu la propriété importante de la classe {@link AttributeAndValueDto} selon laquelle c'est
 * le couple entre sa propriété {@link AttributeAndValueDto#getAttributeCode()} et sa propriété {@link AttributeAndValueDto#getAttributeValueCode()} qui sert
 * pour l'égalité et le hashcode (les algorithme de la présente application s'appuient sur cette propriété).
 *
 * @author p_nlegriel
 */
public class AttributeAndValueDtoTest {

    @Test
    public void veriryThatAttributeAndValueDtoIsRetrievableInSetOnlyByCodeAndByName() {
        // Given
        final String code = "code";
        final String anotherCode = "another" + code;
        final String valueCode = "valueCode";
        final String anotherValueCode = "another" + valueCode;

        final AttributeAndValueDto attributeAndValueDto = new AttributeAndValueDto(code, "name", valueCode, "valueName");

        Set<AttributeAndValueDto> attributesWithTheirValues = new HashSet<>();
        attributesWithTheirValues.add(attributeAndValueDto);

        // When and then
        assertTrue(attributesWithTheirValues.contains(attributeAndValueDto));
        assertTrue(attributesWithTheirValues.contains(new AttributeAndValueDto(code, "name", valueCode, "valueName")));
        assertFalse(attributesWithTheirValues.contains(new AttributeAndValueDto(anotherCode, "name", valueCode, "valueName")));
        assertFalse(attributesWithTheirValues.contains(new AttributeAndValueDto(code, "name", anotherValueCode, "valueName")));
        assertFalse(attributesWithTheirValues.contains(new AttributeAndValueDto(anotherCode, "name", anotherValueCode, "valueName")));
    }
}
