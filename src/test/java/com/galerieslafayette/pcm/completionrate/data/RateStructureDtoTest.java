package com.galerieslafayette.pcm.completionrate.data;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import com.galerieslafayette.pcm.completionrate.data.RateStructureDto;

public class RateStructureDtoTest {

    private static final CategoryStructureDto EMPTY_CATEGORY = new CategoryStructureDto("", "", "", "", "", "", "", "");
    private static final AttributeAndValueDto EMPTY_ATTRIBUTE_AND_VALUE = new AttributeAndValueDto("", "", "", "");

    @Test(expected = IllegalArgumentException.class)
    public void constructorMustNotHaveFirstParameterNull() {
        new RateStructureDto(EMPTY_CATEGORY, null, false, 0, 0, 0, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorMustNotHaveSecondParameterNull() {
        new RateStructureDto(null, EMPTY_ATTRIBUTE_AND_VALUE, false, 0, 0, 0, 0);
    }

    @Test
    public void getUniverseOrPfOrPsfOrPssfShouldConcatenateCodeAndCategoryName() {
        CategoryStructureDto categoryStructure = new CategoryStructureDto("UNIVERSE_CODE", "UNIVERSE_NAME", "PF_CODE", "PF_NAME", "PSF_CODE", "PSF_NAME",
                "PSSF_CODE", "PSSF_NAME");
        RateStructureDto rateStructure = new RateStructureDto(categoryStructure, EMPTY_ATTRIBUTE_AND_VALUE, false, 0, 0, 0, 0);

        assertThat(rateStructure.getPssfLabel()).isEqualTo(categoryStructure.getPssfCode() + " - " + categoryStructure.getPssfName());
        assertThat(rateStructure.getPsfLabel()).isEqualTo(categoryStructure.getPsfCode() + " - " + categoryStructure.getPsfName());
        assertThat(rateStructure.getPfLabel()).isEqualTo(categoryStructure.getPfCode() + " - " + categoryStructure.getPfName());
        assertThat(rateStructure.getUniverseLabel()).isEqualTo(categoryStructure.getUniverseCode() + " - " + categoryStructure.getUniverseName());

        assertThat(rateStructure.getPssf()).isEqualTo(categoryStructure.getPssfCode());
    }

    @Test
    public void checkAttributeAndValueCodesAndLabels() {
        AttributeAndValueDto attributeAndValue = new AttributeAndValueDto("ATTR_CODE", "ATTR_NAME", "ATTR_VAL_CODE", "ATTR_VAL_NAME");
        RateStructureDto rateStructure = new RateStructureDto(EMPTY_CATEGORY, attributeAndValue, false, 0, 0, 0, 0);

        assertThat(rateStructure.getAttributeCode()).isEqualTo(attributeAndValue.getAttributeCode());
        assertThat(rateStructure.getAttributeName()).isEqualTo(attributeAndValue.getAttributeName());
        assertThat(rateStructure.getAttributeValueCode()).isEqualTo(attributeAndValue.getAttributeValueCode());
        assertThat(rateStructure.getAttributeValueName()).isEqualTo(attributeAndValue.getAttributeValueName());
    }

    @Test
    public void getRateXxxShouldReturnNsWhenAllValuesAreZero() {
        RateStructureDto rateStructure = new RateStructureDto(EMPTY_CATEGORY, EMPTY_ATTRIBUTE_AND_VALUE, false, 0, 0, 0, 0);

        assertThat(rateStructure.getValuatedMocosRate()).isEqualTo(RateStructureDto.NS_VALUE);
        assertThat(rateStructure.getValuatedProductsRate()).isEqualTo(RateStructureDto.NS_VALUE);
    }

    @Test
    public void getRateXxxShouldReturn0PercentWhenDenominatorsAreNotZero() {
        RateStructureDto rateStructure = new RateStructureDto(EMPTY_CATEGORY, EMPTY_ATTRIBUTE_AND_VALUE, false, 1, 1, 0, 0);

        assertThat(rateStructure.getValuatedMocosRate()).isEqualTo("0.0 %");
        assertThat(rateStructure.getValuatedProductsRate()).isEqualTo("0.0 %");
    }

    @Test
    public void getRateXxxShouldDisplay100PercentWhenDenominatorEqualsNumerator() {
        RateStructureDto rateStructure = new RateStructureDto(EMPTY_CATEGORY, EMPTY_ATTRIBUTE_AND_VALUE, false, 1, 1, 1, 1);

        assertThat(rateStructure.getValuatedMocosRate()).isEqualTo("100.0 %");
        assertThat(rateStructure.getValuatedProductsRate()).isEqualTo("100.0 %");
    }

    @Test
    public void getRateXxxShouldDisplay50PercentWhenNumeratorEqualsHalfOfNumerator() {
        RateStructureDto rateStructure = new RateStructureDto(EMPTY_CATEGORY, EMPTY_ATTRIBUTE_AND_VALUE, false, 2, 2, 1, 1);

        assertThat(rateStructure.getValuatedMocosRate()).isEqualTo("50.0 %");
        assertThat(rateStructure.getValuatedProductsRate()).isEqualTo("50.0 %");
    }

    @Test
    public void getRateXxxShouldRoundToOneDecimal() {
        RateStructureDto rateStructure = new RateStructureDto(EMPTY_CATEGORY, EMPTY_ATTRIBUTE_AND_VALUE, false, 3, 3, 1, 1);

        assertThat(rateStructure.getValuatedMocosRate()).isEqualTo("33.3 %");
        assertThat(rateStructure.getValuatedProductsRate()).isEqualTo("33.3 %");
    }

    @Test
    public void equalityBetweenRateStructureDtoIsOnPssfAttributeAndAttributeValue() {
        final String pssf = "PSSF";
        final CategoryStructureDto categoryStructure = new CategoryStructureDto("PU", "PU Name", "PF", "PF Name", "PSF", "PSF Name", pssf, "PSSF Name");
        final CategoryStructureDto categoryStructureDiffentOnPssf = new CategoryStructureDto("PU", "PU Name", "PF", "PF Name", "PSF", "PSF Name",
                pssf + "Other", "PSSF Name");

        final String attributeCode = "ATTR";
        final String attributeValueCode = "ATTR_VAL";
        final AttributeAndValueDto attributeAndValue = new AttributeAndValueDto(attributeCode, "ATTR Name", attributeValueCode, "ATTR_VAL Name");
        final AttributeAndValueDto attributeAndValueDifferentOnAttributeCode = new AttributeAndValueDto(attributeCode + "Other", "ATTR Name",
                attributeValueCode, "ATTR_VAL Name");
        final AttributeAndValueDto attributeAndValueDifferentOnAttributeValueCode = new AttributeAndValueDto(attributeCode, "ATTR Name",
                attributeValueCode + "Other", "ATTR_VAL Name");

        final RateStructureDto rateStructure = new RateStructureDto(categoryStructure, attributeAndValue, false, 1, 2, 3, 4);
        final RateStructureDto rateStructureDifferentOnPssf = new RateStructureDto(categoryStructureDiffentOnPssf, attributeAndValue, false, 1, 2, 3, 4);
        final RateStructureDto rateStructureDifferentOnAttributeCode = new RateStructureDto(categoryStructure, attributeAndValueDifferentOnAttributeCode, false,
                1, 2, 3, 4);
        final RateStructureDto rateStructureDifferentOnAttributeValueCode = new RateStructureDto(categoryStructure,
                attributeAndValueDifferentOnAttributeValueCode, false, 1, 2, 3, 4);
        final RateStructureDto rateStructureDifferentOnAllButNotPssfNeitherAttributeCodeNeitherAttributeValueCode = new RateStructureDto(
                new CategoryStructureDto("other", "other", "other", "other", "other", "other", pssf, "other"),
                new AttributeAndValueDto(attributeCode, "other", attributeValueCode, "other"), false, 5, 6, 7, 8);

        assertThat(rateStructure.hashCode()).isEqualTo(rateStructure.hashCode());
        assertThat(rateStructure.hashCode()).isEqualTo(rateStructureDifferentOnAllButNotPssfNeitherAttributeCodeNeitherAttributeValueCode.hashCode());
        assertThat(rateStructure.hashCode()).isNotEqualTo(rateStructureDifferentOnAttributeCode.hashCode());
        assertThat(rateStructure.hashCode()).isNotEqualTo(rateStructureDifferentOnPssf.hashCode());
        assertThat(rateStructure.hashCode()).isNotEqualTo(rateStructureDifferentOnAttributeValueCode.hashCode());

        assertThat(rateStructure).isEqualTo(rateStructureDifferentOnAllButNotPssfNeitherAttributeCodeNeitherAttributeValueCode);
        assertThat(rateStructure).isNotEqualTo(rateStructureDifferentOnAttributeCode);
        assertThat(rateStructure).isNotEqualTo(rateStructureDifferentOnPssf);
        assertThat(rateStructure).isNotEqualTo(rateStructureDifferentOnAttributeValueCode);

        assertThat(rateStructure).isNotEqualTo(null);
        assertThat(rateStructure).isEqualTo(rateStructure);
        assertThat(rateStructure).isNotEqualTo("Object of another type");
    }
}
