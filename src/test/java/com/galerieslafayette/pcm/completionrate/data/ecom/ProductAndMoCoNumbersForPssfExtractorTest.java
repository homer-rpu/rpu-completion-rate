package com.galerieslafayette.pcm.completionrate.data.ecom;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;

@RunWith(MockitoJUnitRunner.class)
public class ProductAndMoCoNumbersForPssfExtractorTest {

    @Mock
    private NamedParameterJdbcOperations ecomJdbcTemplate;

    private ProductAndMoCoNumbersForPssfExtractor productsCounterRowMapper;

    @Before
    public void setUp() {
        productsCounterRowMapper = new ProductAndMoCoNumbersForPssfExtractor(ecomJdbcTemplate);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void productsNumberAnMoCoNumberShouldAppearInResult() {
        // Given
        final int productsNumber = 5;
        final int moCosNumber = productsNumber + 15;
        when(ecomJdbcTemplate.query(anyString(), any(MapSqlParameterSource.class), Mockito.<RowMapper<Integer>>any())).thenReturn(singletonList(productsNumber),
                singletonList(moCosNumber));

        // When and Then
        assertThat(productsCounterRowMapper.extractProductsNumber("PSSF", new HashSet<>(asList("SEASONS_FILTER")))).isEqualTo(productsNumber);
        assertThat(productsCounterRowMapper.extractMoCosNumber("PSSF", new HashSet<>(asList("SEASONS_FILTER")))).isEqualTo(moCosNumber);
    }
}
