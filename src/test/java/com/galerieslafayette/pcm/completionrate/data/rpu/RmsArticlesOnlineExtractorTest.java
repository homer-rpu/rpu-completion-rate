package com.galerieslafayette.pcm.completionrate.data.rpu;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;

@RunWith(MockitoJUnitRunner.class)
public class RmsArticlesOnlineExtractorTest {

    @Mock
    private NamedParameterJdbcOperations rpuJdbcTemplate;

    private RmsArticlesOnlineExtractor extractorUnderTest;

    @Before
    public void setUp() {
        extractorUnderTest = new RmsArticlesOnlineExtractor(rpuJdbcTemplate);
    }

    @Test
    public void resultShouldBeEmptyIfNoOnLineRmsArticlesInDatabase() {
        when(rpuJdbcTemplate.query(anyString(), any(MapSqlParameterSource.class), Mockito.<RowMapper<String>>any())).thenReturn(emptyList());

        assertThat(extractorUnderTest.getAllOnlineGlRmsArticlesUg(new HashSet<>(asList("SEASONS_FILTER")))).isEmpty();
    }

    @Test
    public void resultShouldContainUgsOfRmsArticles() {
        final List<String> ugs = asList("1111", "2222", "3333");
        when(rpuJdbcTemplate.query(anyString(), any(MapSqlParameterSource.class), Mockito.<RowMapper<String>>any())).thenReturn(ugs);

        assertThat(extractorUnderTest.getAllOnlineGlRmsArticlesUg(new HashSet<>(asList("SEASONS_FILTER")))).containsExactlyElementsOf(ugs);
    }
}
