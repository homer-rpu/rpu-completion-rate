package com.galerieslafayette.pcm.completionrate.data.ecom;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.galerieslafayette.pcm.completionrate.data.ecom.ClassificationCategoryAndAttributeValuesForAttributeExtractor;

@RunWith(MockitoJUnitRunner.class)
public class ClassificationCategoryAndAttributeValuesForAttributeExtractorTest {

    @Mock
    private JdbcTemplate ecomJdbcTemplate;

    private ClassificationCategoryAndAttributeValuesForAttributeExtractor extractorUnderTest;

    @Before
    public void setUp() {
        extractorUnderTest = new ClassificationCategoryAndAttributeValuesForAttributeExtractor(ecomJdbcTemplate);
    }

    @Test
    public void ifAttributeIsUnknownResultShouldBeEmpty() {
        when(ecomJdbcTemplate.query(anyString(), any(Object[].class), Mockito.<RowMapper<String>>any())).thenReturn(emptyList());

        assertThat(extractorUnderTest.extractClassificationCategoriesWithTheiAttributeValuesForAttribute("UNKNOWN")).isEmpty();
    }
}
